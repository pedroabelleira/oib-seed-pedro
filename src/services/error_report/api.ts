/**
 * Represents an application error report
 */
export interface IErrorReport {
    id: string;
    user: string;
    url: string;
    environment: string;
    message: string;
    userComments: string;
}

/**
 * Service which allow to send error reports
 */
export interface IErrorReportService {
    submitReport(err: IErrorReport);

    /**
     * Creates a new error report with the id, user, url and environment fields
     * already filled in
     */
    createEmptyErrorReport();
}
