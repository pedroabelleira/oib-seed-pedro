export interface ISearchQuery {
    /**
     * String to search for
     */
    query: string;

    /**
     * Array of the types of object to include in the result. If empty or
     * undefined all types of objects will be included in the search
     */
    objectTypes?: string[], // arrays of object types to include in the results

    /**
     * Minimum creation date for the object to be searched for. If undefined or
     * null no minimum creation date restriction will be applied
     */
    createdFrom?: Date,

    /**
     * Maximum creation date for the object to be searched for. If undefined or
     * null no maximum creation date restriction will be applied
     */
    createdUntil?: Date,

    /**
     * Minimum modification date for the object to be searched for. If undefined or
     * null no minimum modification date restriction will be applied
     */
    modifiedFrom?: Date,

    /**
     * Maximum modification date for the object to be searched for. If undefined or
     * null no maximum modification date restriction will be applied
     */
    modifiedUntil?: Date
}

export interface ISearchResult {
    /**
     * Unique id for this result (for the given objectType)
     */
    id: string;

    /**
     * Url where to direct the user to in order to view/edit/... the result
     */
    url: string;

    /**
     * Type of object. The semantics are application specific
     */
    objectType: string;

    /**
     * Date and time when the object was created
     */
    creationDate: Date;

    /**
     * Date and time when the object was modified
     */
    modificationDate: Date;

    /**
     * Short string (10-20 chars) representing the object
     */
    name: string;

    /**
     * String (100-200 chars) summarising the information inside the object
     * It should not overlap the information in the 'name' as for this
     * information to be able to be shown to the user next to the name
     */
    summary: string;
}

export interface ISearchService {
    search(query: ISearchQuery): Promise<ISearchResult>;
}
