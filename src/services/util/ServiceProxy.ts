// WTF??
declare class Proxy{
    constructor(target, handler);
};

export function createServiceProxy(service: any): IServiceProxy {
    var ret:any = new Proxy(service, {
        get: (target, name, receiver) => {
            return function () {
                console.log('Before calling method %s on service %s', name, service);
                Reflect.get(target, name).apply(service, arguments);
                // Reflect.get(target, name).apply(service, arguments);
                console.log('After Calling a method on a service: ' + name);
                //
            }
        }
    });

    return ret;
}

export interface IServiceProxy {
    serviceName(): string;
    serviceDescription(): string;
    availableImplementations(): IServiceProxy[];
    currentImplementation(): IServiceProxy[];
    changeImplementation(newImplementation: IServiceProxy);
}
