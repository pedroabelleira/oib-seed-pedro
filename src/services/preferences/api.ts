export enum PreferenceScope {Session, Browser, Server}

/**
 * Service which allows to retrieve and set user preferences. There are three
 * possible scopes:
 * Session: the value will be kept for the current session and lost otherwise
 * Browser: the value will be kep for the current browser (it set a cookie)
 * Server: the value will be saved in a persisten store at the server side
 * (at the database)
 */
export interface IUserPreferences {
    /**
     * Retrieves the preference, searching in the Session, Browser and Server
     * scope (in this order). The promise resolves to undefined if the
     * preference has not been set in any of the scopes
     */
    getPreference(key: string): Promise<any>;

    /**
     * Sets the preference to the given value in the specified scope. The
     * caller can send a callback which is called in case the preference
     * could not been set
     */
    setPreference(key: string, value: any, scope?: PreferenceScope, onError?: (string) => void): void;
}
