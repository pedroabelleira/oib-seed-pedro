import {IUserPreferences, PreferenceScope} from './api';

/**
 * Mock implementation of IUserPreferences which stores the preference for any
 * scope in an internal object (=> all scopes are the same)
 */
export class UserPreferencesMock implements IUserPreferences {
    private preferences = {};

    getPreference(key: string): Promise<any> {
        let ret = this.preferences[key];

        return new Promise((resolve, reject) => {
            resolve(ret);
        });
    }

    setPreference(key: string, value: any, scope: PreferenceScope, onError?: (string) => void): void {
        this.preferences[key] = value;
    }
}
