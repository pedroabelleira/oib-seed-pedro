'use strict';

import {IGuardian, IUser} from '../api.ts';

/** Mock implementation for the IGuardian interface */
export class GuardianMock implements IGuardian {
    private user: IUser =  {
        id: '23665',
        fullName: 'Pedro Abelleira Seco',
        name: 'Pedro',
        surname: 'Abelleira Seco',
        roles: ["ADMIN", "PRESENSE_MNG"]
    };

    userHasRole(role: string): boolean {
        return this.user.roles.indexOf(role) != -1;
    }

    userIsLogged(): boolean {
        return true;
    }

    getLoggedUser(): IUser {
        return Object.freeze(this.user);
    }

    addRole(role: string) {
        this.user.roles.push(role);
    }

    removeRole(role: string) {
        let index: number = this.user.roles.indexOf(role);
        if (index != -1) {
            this.user.roles.splice(index, 1);
        }
    }
}
