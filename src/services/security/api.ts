/**
 * Gateway to the security services which control access to the functionality
 * by the current user
 */
export interface IGuardian {
    /**
     * Returns true if the user has the role, false otherwise
     * If an error occurs, it will be logged in the console and false will
     * be returned. No exceptions are thrown
     */
    userHasRole(role: string): boolean;

    /**
     * Returns true if the user is currently logged in the system, false \
     * otherwise.
     * If an error occurs, it will be logged in the console and false will
     * be returned. No exceptions are thrown
     */
    userIsLogged(): boolean;

    /**
     * Returns an inmutable user object representing the currently logged
     * user, if it exists. If the the user is not currently logged, it
     * will return undefined
     * If an error occurs, it will be logged in the console and undefined
     * will be returned. No exceptions are thrown
     */
    getLoggedUser(): IUser;
}

/** Represents the information of the current user */
export interface IUser {
    /** Inmutable property representing the user id */
    id: string;

    /** Inmutable property representing the user full name */
    fullName: string;

    /** Inmutable property representing the user name */
    name: string;

    /** Inmutable property representing the user surname */
    surname: string;

    /** List of roles that this user has currently assigned */
    roles: string[];
}
