/**
 * Translation service
 */
export interface ITranslator {
    /**
     * Returns the translation of the given message id. If the translation
     * is not found or any error occurs, default_text, is returned.
     * If default_text is not provided, implementations will treat msg_id
     * as the default_text.
     * A basic hierarchycal model is supported. If msg_id contains a dot
     * (e.g. msg_id = 'main.title') it's interpreted that the translation
     * of the message with key 'title' of module 'main' is requested
     * Implementations can choose different storage, retrieval, caching...
     * strategies depending on the module
     */
    translate(msg_id: string, default_text?: string): string;

    /**
     * Equivalent to translate(msg_id)
     */
     t(msg_id: string): string;
}
