'use strict';

import {ITranslator} from '../api';

export class TranslatorMock implements ITranslator {
    translate(msg_id: string, default_text?: string): string {
        return '*' + msg_id + '*';
    }

    /**
     * Equivalent to translate(msg_id)
     */
     t(msg_id: string): string {
         return this.translate(msg_id);
     }
}
