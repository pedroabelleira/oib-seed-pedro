'use strict';

import {ITranslator} from '../api';

export class TranslatorNull implements ITranslator {
    translate(msg_id: string, default_text?: string): string {
        return 'null';
    }

    /**
     * Equivalent to translate(msg_id)
     */
     t(msg_id: string): string {
         return this.translate(msg_id);
     }
}
