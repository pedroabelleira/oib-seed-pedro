/**
 * Provide an easy to use interface to the retrieval and management of
 * 'favourite' objects
 */
export interface IFavouritesService {
    addFavourite(fav: IFavourite);
    retrieveFavourites(type?: string);
}

/**
 * Represents a favourite object
 */
export interface IFavourite {
    /**
     * Unique id of the referred object. The semantics of this id are application
     * specific
     */
    id: string;

    /**
     * A string indicating the type of object that this favourite refers to
     * It can be used to retrieve only objects of one or several types
     */
    type: string;

    /**
     * String representing this object in a short human readable format
     */
    name: string;

    /**
     * Complementary string with a summary of the information which this
     * favourite object contains. Should not overlap with the name in order to
     * be able to show the information in a way similar to '*Name* (summary)'
     */
    summary: string;

    /**
     * Router friendly URL which can be used to direct the user towards the
     * favourite object
     */
    url: string;
}
