import {Component, View} from 'angular2/core';
// import {OnDestroy} from 'angular2/common';

@Component({
  selector: 'oib-accordion, [oib-accordion]',
  properties: ['onlyOneOpen: closeOthers']
})
@View({template: `<ng-content></ng-content>`})
export class Accordion {
  private onlyOneOpen: boolean;
  private groups: Array<AccordionGroup> = [];

  addGroup(group: AccordionGroup): void { this.groups.push(group); }

  closeOthers(openGroup): void {
    if (!this.onlyOneOpen) {
      return;
    }

    this.groups.forEach((group: AccordionGroup) => {
      if (group !== openGroup) {
        group.isOpen = false;
      }
    });
  }

  removeGroup(group: AccordionGroup): void {
    const index = this.groups.indexOf(group);
    if (index !== -1) {
      this.groups.splice(index, 1);
    }
  }
}

@Component({selector: 'oib-accordion-group, [oib-accordion-group]', properties: ['heading', 'isOpen', 'isDisabled']})
@View({
  template: `
    <div class="card">
      <div class="card-header">
        <a href tabindex="0"><span [class.text-muted]="isDisabled" (click)="toggleOpen($event)">{{heading}}</span></a>
      </div>
      <div class="card-block" [hidden]="!isOpen">
        <div class="card-text">
            <ng-content></ng-content>
        </div>
      </div>
    </div>
  `
})
export class AccordionGroup /*implements OnDestroy*/ {
  private isDisabled: boolean;
  private _isOpen: boolean = false;

  constructor(private accordion: Accordion) { this.accordion.addGroup(this); }

  toggleOpen(event) {
    event.preventDefault();
    if (!this.isDisabled) {
      this.isOpen = !this.isOpen;
    }
  }

  onDestroy(): void { this.accordion.removeGroup(this); }

  public get isOpen(): boolean { return this._isOpen; }

  public set isOpen(value: boolean) {
    this._isOpen = value;
    if (value) {
      this.accordion.closeOthers(this);
    }
  }
}
