import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {NgFor, NgIf} from 'angular2/common';

@Component({
    selector: 'oib-tabs',
    styleUrls: ['components/tabs/tabs.css'],
    templateUrl: 'components/tabs/tabs.html',
    directives: [NgFor, NgIf]
})
export class Tabs {
    @Input() areTabsExpandable: boolean | string;
    @Input() areTabsExpanded: boolean = false;
    @Output() private onTabsExpanded = new EventEmitter<boolean>();

    expanderHidden: boolean = true; // Aux variable to hide expander
    tabs: Tab[];

    constructor() {
        this.tabs = [];
        if (typeof this.areTabsExpandable === 'string') { // FIXME: this code doesn't work as it's supposed to
            this.areTabsExpandable = (this.areTabsExpandable !== 'false');
        } else {
            this.areTabsExpandable || (this.areTabsExpandable = false);
        }
    }

    selectTab(tab) {
        _deactivateAllTabs(this.tabs);
        tab.active = true;

        function _deactivateAllTabs(tabs: Tab[]) {
            tabs.forEach((tab) => tab.active = false);
        }
        tab._onTabSelected();

        return false; // prevent event from being further propagated
    }

    addTab(tab: Tab) {
        if (this.tabs.length === 0) {
            tab.active = true;
        }
        this.tabs.push(tab);
    }

    onTabExpandToggle() {
        this.areTabsExpanded = !this.areTabsExpanded;
        this.onTabsExpanded.emit(this.areTabsExpanded);
        return false;
    }

    onMouseOver() {
        this.expanderHidden = false;
    }

    onMouseOut() {
        this.expanderHidden = true;
    }

    isExpanderHidden() {
        return this.expanderHidden;
    }
}

@Component({
    selector: 'oib-tab',
    inputs: [
        'title:tabTitle',
        'active'
    ],
    styleUrls: ['components/tabs/tabs.css'],
    templateUrl: 'components/tabs/tab.html'
})
export class Tab {
    @Output() private onTabSelected = new EventEmitter();

    title: string;
    active = this.active || false;
    expanderHidden: boolean = true; // Aux variable to hide expander

    constructor(private tabs: Tabs) {
        tabs.addTab(this);
    }

    isActive(): boolean {
        return this.active || this.tabs.areTabsExpanded;
    }

    showHeader(): boolean {
        return this.tabs.areTabsExpanded;
    }

    collapse(): boolean {
        this.tabs.onTabExpandToggle();
        return false;
    }

    onMouseOver() {
        this.expanderHidden = false;
    }

    onMouseOut() {
        this.expanderHidden = true;
    }

    isExpanderHidden() {
        return this.expanderHidden;
    }

    _onTabSelected() {
        this.onTabSelected.emit('tab');
    }
}
