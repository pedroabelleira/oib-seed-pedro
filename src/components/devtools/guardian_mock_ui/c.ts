import {Component, Inject} from 'angular2/core';
import {IGuardian, IUser} from '../../../services/security/api';
import {GuardianMock} from '../../../services/security/mock/GuardianMock';

/**
 * Development component which allows the developer to change user roles on the
 * fly
 */
@Component({
    selector: 'dev-guardian-mock',
    templateUrl: 'components/devtools/guardian_mock_ui/t.html',
    styleUrls: ['components/devtools/guardian_mock_ui/s.css'],
    directives: []
})
export class CmpDevGuardianMock {
    private _roles: string[];
    private _user: IUser;

    constructor( @Inject('IGuardian') public guardian: GuardianMock) {
    }

    ngOnInit() {
        this._roles = this.guardian.getLoggedUser().roles;
        this._user= this.guardian.getLoggedUser();
    }

    /*
    * @returns return false to prevent default form submit behavior to refresh the page.
    */
    add(role): boolean {
        if (role.value === '') return;
        this.guardian.addRole(role.value);
        role.value = '';
        return false;
    }

    remove(role: string) {
        this.guardian.removeRole(role);
        return false;
    }
}
