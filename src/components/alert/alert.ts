import { Component, Input } from 'angular2/core';

@Component({
    selector: 'oib-alert',
    providers: [],
    templateUrl: 'components/alert/alert.html',
    // styleUrls: ['app/oib/components/alert.css'],
    directives: [ ],
    pipes: []
})
export class Alert {
    @Input() alertType: string;
}
