import {Component, Input, Output, EventEmitter, Inject, ElementRef, ContentChildren, QueryList} from 'angular2/core';
import {ITreeNode, ModelTreeNode} from './ModelTreeNode';

@Component({
    selector: 'oib-treenode',
    templateUrl: 'components/treenode/TreeNode.html',
    directives: [ModelTreeNode]
})
export class TreeNode {
    @Input() object: any;
    @Input() text: string;
    @Input() isExpanded: boolean;
    @Input() nodeHidden: boolean;
    @Output() onNodeSelected = new EventEmitter();
    @ContentChildren(TreeNode) childrenList: QueryList<TreeNode>;

    parent: TreeNode;
    children: TreeNode[];
    model: ITreeNode;
    isRoot: boolean;

    constructor() {
    }

    ngAfterContentInit() {
        let tnodes = this.childrenList.toArray();
        let tmodel = [];
        tnodes = tnodes.splice(1); // first child is this node!

        tnodes.forEach(node => {
            node.setParent(this);
        });
        this.children = tnodes;

        this.isRoot = typeof this.parent === 'undefined';

        if (this.isRoot == false) {
            this.model = null;
        } else {
            this.model = this.createModel();
        }
    }

    createModel() {
        let ret = {
            text: this.text,
            object: this.object,
            onNodeSelected: () => {this._doOnNodeSelected()},
            children: [],
            isExpanded: this.isExpanded,
            isSelected: false
        };

        if (this.nodeHidden) {
            ret['nodeHidden'] = this.nodeHidden;
        }

        this.children.forEach( (child) => {
            ret.children.push(child.createModel());
        });

        return ret;
    }

    setParent(parent: TreeNode) {
        this.parent = parent;
    }

    private _doOnNodeSelected() {
        let em: EventEmitter<any> = this.onNodeSelected;

        if (em) {
            em.emit(this.object);
        }
    }
}
