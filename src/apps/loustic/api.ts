/// <reference path="../../typings/tsd.d.ts" />

import {bootstrap} from 'angular2/platform/browser';
import {provide, Inject, Injectable} from 'angular2/core';
import {ROUTER_PROVIDERS, APP_BASE_HREF, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {App} from './app';

/** Service interfaces */
import {ITranslator} from '../../services/translation/api';
import {IGuardian} from '../../services/security/api';
import {IChildrenService} from './services/repository/api';
import {IUserPreferences} from '../../services/preferences/api';

/** Service implementations for development */
import {TranslatorNull} from '../../services/translation/mock/TranslatorNull';
import {GuardianMock} from '../../services/security/mock/GuardianMock';
import {ChildrenServiceMock} from './services/repository/mock';
import {UserPreferencesMock} from '../../services/preferences/mock';

export interface ILoustic {
    translator: ITranslator;
    // Alias to translator
    t: ITranslator;

    guardian: IGuardian;
    // Alias to guardian
    g: IGuardian;

    childrenService: IChildrenService;
    // Alias to childrenService
    ch: IChildrenService;

    userPreferences: IUserPreferences;
    // Alias to userPreferencesService
    p: IUserPreferences;
}

@Injectable()
export class Loustic implements ILoustic {
    t: ITranslator;
    g: IGuardian;
    ch: IChildrenService;
    p: IUserPreferences;

    constructor(
        @Inject('IChildrenService') public childrenService: IChildrenService,
        @Inject('IGuardian') public guardian: IGuardian,
        @Inject('ITranslator') public translator: ITranslator,
        @Inject('IUserPreferences') public userPreferences: IUserPreferences
    ) {
        this.t = translator;
        this.g = guardian;
        this.ch = childrenService;
        this.p= userPreferences;
    }
}
