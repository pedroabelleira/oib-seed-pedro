'use strict';

/**
 * Represents the data of a Child. The data is read-write, but
 * changes won't be saved unless it is explicitly requested to do so
 */
export interface IChild {
    id: string;
    name: string;
    surname: string;
    birth_date: Date;
    school: string;
    waiting: boolean;
    exit_date: Date;
}

/**
 * Represents the data of a Section. The data is read-write, but
 * changes won't be saved unless it is explicitly requested to do so
 */
export interface ISection {
    id: string;
    name: string;
}

/**
 * Service which provides access to information and methods to modify it. The
 * API is generally asynchronous and uses promises
 */
export interface IChildrenService {
    /**
     * Returns all the children who belong to the
     * section with the given section_id.
     */
    getChildren(section_id: string): Promise<IChild[]>;

    /**
     * Returns the child with the given child_id. It fails with an error
     * (rejected promise) if the child is not found or any other error
     * takes place
     */
    findChildById(child_id: string): Promise<IChild>;

    /**
     * Get the list of all the sections available for the current logged user
     */
    getSections(): Promise<ISection[]>;
}
