'use strict';

import {ISection, IChild, IChildrenService} from './api';

declare var faker: any;

interface ISectionMock extends ISection {
    children: IChild[];
}

export class ChildrenServiceMock implements IChildrenService {
    private children1: IChild[] = [
        {
            id: '1',
            name: 'Agnieszka',
            surname: 'Virkska',
            birth_date: new Date(),
            school: 'school',
            waiting: true,
            exit_date: new Date()
        },
        {
            id: '2',
            name: faker.name.firstName(),
            surname: faker.name.lastName(),
            birth_date: faker.date.between(new Date(2007, 0, 1), new Date(2013, 12, 31)),
            school: 'school Antonio',
            waiting: faker.random.boolean(),
            exit_date: new Date()
        }
    ];


    private sections: ISectionMock[] = [
        {
            id: '1',
            name: 'Section 1',
            children: this.children1
        },
        {
            id: '2',
            name: 'Section 2',
            children: this.children1
        }
    ];

    getSections(): Promise<ISection[]> {
        let that = this;
        return new Promise((resolve, reject) => {
            resolve(that.sections);
        });
    }

    getChildren(section_id: string): Promise<IChild[]> {
        let that = this;
        return new Promise((resolve, reject) => {
            // var s: ISectionMock = this.getSection(section_id);
            var s: ISectionMock = that._getSection(section_id);

            if (s) {
                resolve(s.children);
            } else {
                reject('Section not found');
            }
        });
    }

    public findChildById(child_id: string): Promise<IChild> {
        return new Promise((resolve, reject) => {
            var i: number;
            var children: IChild[] = this._getAllChildren();

            for(i = 0; i < children.length; i++) {
                var c: IChild = children[i];
                if (c.id === child_id) {
                    break;
                }
            }

            if (c) {
                resolve(c);
            } else {
                reject('Child not found');
            }
        });
    }

    private _getAllChildren(): IChild[] {
        var i: number;
        var ret: IChild[] = [];

        for(i = 0; i < this.sections.length; i++) {
            ret = ret.concat(this.sections[i].children);
        }

        return ret;
    }

    private _getSection(section_id: string): ISectionMock {
        var i: number;

        for(i = 0; i < this.sections.length; i++) {
            var s: ISectionMock = this.sections[i];
            if (s.id === section_id) {
                return s;
            }
        }
        return undefined;
    }
}
