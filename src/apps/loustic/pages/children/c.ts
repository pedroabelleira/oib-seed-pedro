import {Component, Input, Output, EventEmitter, Inject} from 'angular2/core';
import {RouterLink} from 'angular2/router';
import {ILoustic} from '../../api';

@Component({
    selector: 'children',
    templateUrl: 'apps/loustic/pages/children/t.html',
    // styleUrls: ['apps/loustic/pages/children/s.css'],
    directives: [RouterLink]
})
export class PageChildren {
    private children;
    private filterText = '';

    constructor(
        @Inject('ILoustic') public loustic: ILoustic)
    {}

    ngOnInit() {
        this.loustic.ch.getChildren('1').then(children => {
            this.children = children;
        });
    }

    ngOnChange() {
        
    }
}
