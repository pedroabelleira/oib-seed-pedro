import { Component } from 'angular2/core';

@Component({
    selector: 'home',
    providers: [],
    templateUrl: 'apps/loustic/pages/home/t.html',
    styleUrls: ['apps/loustic/pages/home/s.css'],
    directives: [ ],
    pipes: []
})
export class PageHome {

}
