import {Component, Input, Output, EventEmitter, Inject} from 'angular2/core';
import {RouterLink} from 'angular2/router';
import {ILoustic} from '../../api';

@Component({
    selector: 'sites',
    template: '<h1>Sites (TODO)</h1>',
    // templateUrl: 'apps/loustic/pages/children/t.html',
    // styleUrls: 'apps/loustic/pages/children/s.css',
    directives: []
})
export class PageSites {
    constructor(
        @Inject('ILoustic') public loustic: ILoustic)
    {}

    ngOnInit() {
        // Init code should go here
    }
}
