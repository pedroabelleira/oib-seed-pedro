import {Component, Input, Output, EventEmitter, Inject} from 'angular2/core';
import {NgSwitch, NgSwitchWhen, NgSwitchDefault} from 'angular2/common';
import {RouteParams} from 'angular2/router';
import {ILoustic} from '../../../api';

@Component({
    selector: 'child-details',
    providers: [],
    templateUrl: 'apps/loustic/pages/child/details/t.html',
    directives: [NgSwitch, NgSwitchWhen, NgSwitchDefault],
    pipes: []
})
export class ChildDetails {
    @Input() private child;

    constructor(
        private _routeParams: RouteParams,
        @Inject('ILoustic') private loustic: ILoustic
    ) {}

    isDefined() {
        if (typeof this.child === 'undefined') {
            return false;
        }
        return true;
    }
}
