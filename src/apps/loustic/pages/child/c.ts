import {Component, Input, Output, EventEmitter, Inject} from 'angular2/core';
import {NgSwitch, NgSwitchWhen, NgSwitchDefault} from 'angular2/common';
import {RouteParams} from 'angular2/router';
import {ILoustic} from '../../api';
import {Tabs, Tab} from '../../../../components/tabs/c';
import {ChildDetails} from './details/c';

const PREFERENCE: string = 'loustic.pages.child.tabsExpanded';

@Component({
    selector: 'child',
    providers: [],
    templateUrl: 'apps/loustic/pages/child/t.html',
    directives: [Tabs, Tab, NgSwitch, NgSwitchWhen, NgSwitchDefault, ChildDetails],
    pipes: []
})
export class PageChild {
    private child;
    private loading: boolean = true;
    private showComment: boolean = false;
    private areTabsExpanded: boolean = false;

    constructor(
        private _routeParams: RouteParams,
        @Inject('ILoustic') private loustic: ILoustic
    ) {}

    ngOnInit() {
        let id = this._routeParams.get('id');
        this.loustic.ch.findChildById(id).then(child => {
            this.child = child;
            this.loading = false;
        });
        this.loustic.p.getPreference(PREFERENCE).then(val => {
            this.areTabsExpanded = val;
        });

        console.log('biribiribir');
    }

    isDefined() {
        if (typeof this.child === 'undefined') {
            return false;
        }
        return true;
    }

    // Called when the user has changed the status of the expansion of the
    // tabs
    onTabsExpanded(val: boolean) {
        this.loustic.p.setPreference(PREFERENCE, val);
    }
}
