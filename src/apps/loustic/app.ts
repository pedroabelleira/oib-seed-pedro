import {Component} from 'angular2/core';
import {RouteConfig, RouterOutlet, ROUTER_PROVIDERS} from 'angular2/router';
import {LousticNavigation} from './navigation/c';

import {PageHome} from './pages/home/c';
import {PageChildren} from './pages/children/c';
import {PageChild} from './pages/child/c';
import {PageParents} from './pages/parents/c';
import {PageSites} from './pages/sites/c';
import {PageFavourites} from './pages/favourites/c';

@Component({
    selector: 'loustic',
    providers: [],
    templateUrl: './apps/loustic/app.html',
    styleUrls: ['./apps/loustic/app.css'],
    directives: [LousticNavigation, RouterOutlet],
    pipes: []
})
@RouteConfig([
    { path: '/',            name: 'Home',           component: PageHome },
    { path: '/children',    name: 'Children',       component: PageChildren },
    { path: '/child/:id',   name: 'Child',          component: PageChild },
    { path: '/parents',     name: 'Parents',        component: PageParents},
    { path: '/sites',       name: 'Sites',          component: PageSites},
    { path: '/favourites',  name: 'Favourites',     component: PageFavourites},
    // { path: '/forge',    name: 'HeroDetail', component: CMPForge }
    { path: '/foo',     name: 'Foo', component: PageHome }
])
export class App {
}
