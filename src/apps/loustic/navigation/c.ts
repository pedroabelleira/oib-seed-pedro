import {Component} from 'angular2/core';
import {Alert} from '../../../components/alert/alert';
import {LeftNav} from './left-nav/c';

@Component({
  selector: 'loustic-navigation',
  providers: [],
  templateUrl: 'apps/loustic/navigation/t.html',
  styleUrls: ['apps/loustic/navigation/s.css'],
  directives: [LeftNav]
})
export class LousticNavigation {
}
