import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {RouterLink} from 'angular2/router';

@Component({
  selector: 'left-nav',
  providers: [],
  templateUrl: 'apps/loustic/navigation/left-nav/t.html',
  styleUrls: ['apps/loustic/navigation/left-nav/s.css'],
  directives: [RouterLink]
})
export class LeftNav {
}
