/// <reference path="../../typings/tsd.d.ts" />

import {bootstrap} from 'angular2/platform/browser';
import {provide, Inject, Injectable} from 'angular2/core';
import {ROUTER_PROVIDERS, APP_BASE_HREF, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {App} from './app';

/** Service interfaces */
import {ILoustic, Loustic} from './api';
import {ITranslator} from '../../services/translation/api';
import {IGuardian} from '../../services/security/api';
import {IUserPreferences} from '../../services/preferences/api';
import {IChildrenService} from './services/repository/api';

/** Service implementations for development */
import {TranslatorNull} from '../../services/translation/mock/TranslatorNull';
import {GuardianMock} from '../../services/security/mock/GuardianMock';
import {UserPreferencesMock} from '../../services/preferences/mock';
import {ChildrenServiceMock} from './services/repository/mock';

import * as proxy from '../../services/util/ServiceProxy';


bootstrap(App, [
  // provide(APP_BASE_HREF, { useValue: '<%= APP_BASE + APP_DEST %>' } ),
  ROUTER_PROVIDERS,
  provide(LocationStrategy, { useClass: HashLocationStrategy }),
  /** Inject mock implementation of app services in development */
  provide('ITranslator', { useClass: TranslatorNull }),
  provide('IGuardian', { useClass: GuardianMock }),
  provide('IUserPreferences', { useClass: UserPreferencesMock}),
  // provide('IChildrenService', { useValue: proxy.createServiceProxy(new ChildrenServiceMock()) }),
  provide('IChildrenService', { useValue: new ChildrenServiceMock() }),
  provide('ILoustic', { useClass: Loustic })
]);
