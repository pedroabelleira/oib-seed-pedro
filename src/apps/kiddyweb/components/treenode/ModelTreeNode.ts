import {Component, Input, Output, EventEmitter, Inject, ElementRef, ContentChildren, QueryList} from 'angular2/core';

export interface ITreeNode {
    text: string;
    object?: any;
    children?: ITreeNode[];
    onNodeSelected?: () => any;
    isExpanded?: boolean;
    isSelected?: boolean;
    nodeHidden?: boolean;
}

@Component({
    selector: 'oib-model-treenode',
    templateUrl: 'apps/kiddyweb/components/treenode/ModelTreeNode.html',
    styleUrls: ['apps/kiddyweb/components/treenode/ModelTreeNode.css'],
    directives: [ModelTreeNode]
})
export class ModelTreeNode {
    @Input() model: ITreeNode;
    @Input() parentNode: ITreeNode;

    constructor() {
    }

    ngAfterContentInit() {
        if (this.parentNode) {
            this.model['___parentNode'] = this.parentNode;
        }
        if (typeof this.model.isExpanded === 'undefined') {
            this.model.isExpanded = true;
        }
    }

    onClick() {
        this._selectNode();
        return false; // Avoid propagating the event
    }

    toggleExpansion() {
        this.model.isExpanded = !this.model.isExpanded;
        return false;
    }

    hasChildren() {
        return this.model.children? this.model.children.length != 0: false;
    }

    isExpanded() {
        return this.model.isExpanded;
    }

    isSelected() {
        return this.model.isSelected;
    }

    private _selectNode() {
        this._unselectRecursive(this._getRootNode(this.model));
        this.model.isSelected = true;

        if (this.model.onNodeSelected) {
            this.model.onNodeSelected();
        }
    }

    private _getRootNode(node: ITreeNode): ITreeNode {
        if (typeof node['___parentNode'] === 'undefined') {
            return node;
        } else {
            return this._getRootNode(node['___parentNode']);
        }
    }

    private _unselectRecursive(node: ITreeNode) {
        node.isSelected = false;
        if (node.children) node.children.forEach(child => {
            this._unselectRecursive(child);
        });
    }
}
