import {Component, Input, Output, EventEmitter}	from 'angular2/core';
import {IChild} from '../../services/person/person.interface'

declare var $: any;
declare var _: any;

@Component({
	selector: 'child-features',
  	templateUrl: 'apps/kiddyweb/components/child-features/template.html',
  	styleUrls: [ 'apps/kiddyweb/components/child-features/style.css',
		  		 'apps/kiddyweb/components/detail-child/style.css' ]
})
export class ChildFeaturesComponent {
	@Input() child: IChild;
	
	@Output() onChildUpdated = new EventEmitter();
	
	isEditMode: boolean = false;
	private srcChild: IChild;
	
	constructor() {
		this.initComponents();
	}
	
	edit(): void {
		this.srcChild = _.defaults({}, this.child);		// get a copy of the current status
		this.isEditMode = true;
	}
	
	delete(): void { 
		$("#dialog_confirm_1").dialog("open");
	}
	
	save(): void {
		this.isEditMode = false;
		this.onChildUpdated.emit(this.child);			// fire event to confirm changes
	}
	
	cancel(): void {
		this.child = _.defaults({}, this.srcChild);		// restore previous status
		this.isEditMode = false;

		this.onChildUpdated.emit(this.child);			// fire event to undo the changes
	}
	
	private initComponents(): void {
		$('#datePicker2').datetimepicker({
			format: 'DD/MM/YYYY',
			useCurrent: false
		});
		
		// Uggly hack because using jQuery
		// Should have put a (change) property on the HTML element, but that doesn't work
		// The following event is fired when the user selects a date from the pop-up,
		// not when the users types something in the input element.
		// Check the "dateChanged" function for the event related to key strokes.
		$('#datePicker').datetimepicker().on('dp.change', (event) => {
			if (event.date._d) {
				var day = ('00' + event.date._d.getDate()).substr(-2, 2),
					month = ('00' + (event.date._d.getMonth() + 1)).substr(-2, 2),
					year = event.date._d.getFullYear(),
					date = day + '/' + month + '/' + year;
					
				this.child.birthDate = date;
			}
		});
		
		$("#dialog_confirm_1").dialog({
			autoOpen: false,
			resizable: false,
			draggable: false,
			height:170,
			width: 320,
			modal: true,
			buttons: {
				"Oui": function() {
					$(this).dialog("close");		// to be implemented
				},
				"Non": function() {
					$(this).dialog("close");		// to be implemented
				}
			}
		});
	}
}