import {Component, Input, Output, EventEmitter, Inject} from 'angular2/core';
import {ServiceManager} from '../../services/kw-service.service';
import {ETypeSuggestion, ISelectable} from '../../api';

//export enum ETypeSuggestion { CHILD, SITE, HOUSEHOLD };

declare var $: any;
declare var _: any;

@Component({
	selector: 'filter-name',
  templateUrl: 'apps/kiddyweb/components/filter-name/template.html',
  styleUrls: ['apps/kiddyweb/components/filter-name/style.css'],
  providers: [ServiceManager]
})
export class FilterNameComponent {
  // Using a setter because it's a jQuery component which has to be initialised
  // in Javascript (Duh!)
  // Destroying it before is not useful for the first assignment,
  // but needed for subsequent assignments.  
  @Input() set suggestions(suggestions_: ETypeSuggestion) {
    $('#suggestions').typeahead('destroy');
    
    switch (suggestions_) {
      case ETypeSuggestion.CHILD:
        this.srv.peopleSrv.getSuggestionables().then(result => { 
          $('#suggestions').typeahead({
            source: result,
            sorter: function(items) {
              return _.sortBy(items, 'name');
            }
          });
        });
        break;
      case ETypeSuggestion.HOUSEHOLD:
        this.srv.househSrv.getSuggestionables().then(result => {
          $('#suggestions').typeahead({
            source: result,
            sorter: function(items) {
              return _.sortBy(items, 'name');
            }
          });
        });
        break;
      case ETypeSuggestion.SITE:
        this.srv.siteSrv.getSuggestionables().then(result => {
          $('#suggestions').typeahead({
            source: result,
            sorter: function(items) {
              return _.sortBy(items, 'name');
            }
          });
        });
        break;
    }
  }
  
  @Output() onSearchRequested = new EventEmitter();
  @Output() onAdvancedSearchRequested = new EventEmitter();
  
  constructor(private srv: ServiceManager) {
    this.initAutocomplete();
  }

  search(): void {
    var value = $("#suggestions").val();
    this.onSearchRequested.emit(value);
  }
  
  searchAdv(): void {
    var value = $("#suggestions").val();
    this.onAdvancedSearchRequested.emit(value);
  }

  private initAutocomplete(): void {
    $("#suggestions").typeahead({
      source: this.suggestions
    });
  }
}