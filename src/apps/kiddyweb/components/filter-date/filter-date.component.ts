import {Component, Output, EventEmitter} from 'angular2/core';
import {EType, IType} from '../../api';

declare var $: any;

@Component({
  selector: 'filter-date',
  templateUrl: 'apps/kiddyweb/components/filter-date/template.html',
  styleUrls: ['apps/kiddyweb/components/filter-date/style.css']
})
export class FilterDateComponent {
  @Output() onTypeChanged = new EventEmitter();
  @Output() onDateChanged = new EventEmitter();
  
  types: IType[] = [
    { id: EType.ALL, label: 'Tous', isActive: true }, 
    { id: EType.FAVOURITES, label: 'Actuels', isActive: false }, 
    { id: EType.DATE, label: 'Actif au', isActive: false }
  ];
  
  isDatePickerDisabled = true;
  
  constructor() {
    this.initDatePicker();
  }
  
  typeChanged(type): void {
    this.isDatePickerDisabled = type.id !== EType.DATE;
    this.onTypeChanged.emit(type.id);
  }
  
  dateChanged(pattern: string) {
    var date = null;
    
    // if pattern match DD/MM/YYYY, send an event with the date,
    // otherwise send a event with an empty string
    if (pattern.match(/\d{1,2}\/\d{1,2}\/\d{4}/g)) {
      var splitted = pattern.split('/'),
          day = ('00' + splitted[0]).substr(-2, 2),
          month = ('00' + splitted[1]).substr(-2, 2);
          
      date = day + '/' + month + '/' + splitted[2];
    }
    
    this.onDateChanged.emit(date);
  }
  
  private initDatePicker(): void {
    $('#datePicker').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: false
    });
    
    // Uggly hack because using jQuery
    // Should have put a (change) property on the HTML element, but that doesn't work
    // The following event is fired when the user selects a date from the pop-up,
    // not when the users types something in the input element.
    // Check the "dateChanged" function for the event related to key strokes.
    $('#datePicker').datetimepicker().on('dp.change', (event) => {
      if (event.date._d) {
        var day = ('00' + event.date._d.getDate()).substr(-2, 2),
            month = ('00' + (event.date._d.getMonth() + 1)).substr(-2, 2),
            year = event.date._d.getFullYear(),
            date = day + '/' + month + '/' + year;
            
        this.onDateChanged.emit(date);
      }
    });
  }
}