import {Component, Input, Output, EventEmitter}	from 'angular2/core';
import {IChild} from '../../services/person/person.interface'

declare var $: any;
declare var _: any;

@Component({
	selector: 'child-household',
  	templateUrl: 'apps/kiddyweb/components/child-household/template.html',
  	styleUrls: [ 'apps/kiddyweb/components/child-household/style.css',
		  		 'apps/kiddyweb/components/detail-child/style.css' ]
})
export class ChildHouseholdComponent {
	@Input() child: IChild;
	
	@Output() onChildUpdated = new EventEmitter();
	
	isEditMode: boolean = false;
	private srcChild: IChild;
	
	constructor() {
		this.initComponents();
	}
	
	edit(): void {
		this.srcChild = _.defaults({}, this.child);		// get a copy of the current status
		this.isEditMode = true;
	}
	
	delete(): void { 
		$("#dialog_confirm_1").dialog("open");
	}
	
	save(): void {
		this.isEditMode = false;
		this.onChildUpdated.emit(this.child);			// fire event to confirm changes
	}
	
	cancel(): void {
		this.child = _.defaults({}, this.srcChild);		// restore previous status
		this.isEditMode = false;

		this.onChildUpdated.emit(this.child);			// fire event to undo the changes
	}
	
	private initComponents(): void {
		$("#dialog_confirm_1").dialog({
			autoOpen: false,
			resizable: false,
			draggable: false,
			height:170,
			width: 320,
			modal: true,
			buttons: {
				"Oui": function() {
					$(this).dialog("close");		// to be implemented
				},
				"Non": function() {
					$(this).dialog("close");		// to be implemented
				}
			}
		});
	}
}