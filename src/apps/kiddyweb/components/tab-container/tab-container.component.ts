import {Component, Input, Output, EventEmitter}	from 'angular2/core';
import {ServiceManager} from '../../services/kw-service.service';

import {PanelListComponent} from '../panel-list/panel-list.component';
import {TreeNode} from '../treenode/TreeNode';

import {IFilter, ETab} from '../../api';
import {ITypeSite, ISite, IGroup} from '../../services/site/site.interface';
import {IHousehold} from '../../services/household/household.interface';
import {IPerson, IParent, IChild} from '../../services/person/person.interface';


@Component({
	selector: 'tab-container',
  	templateUrl: 'apps/kiddyweb/components/tab-container/template.html',
  	styleUrls: ['apps/kiddyweb/components/tab-container/style.css'],
	directives: [PanelListComponent, TreeNode],
	providers: [ServiceManager]
})
export class TabContainerComponent {
	@Input() set filter(filter: IFilter) {
		if (filter) {
			if (filter.filterTab === ETab.CHILDREN) {
				this.srv.peopleSrv.getChildrenByFilter(filter).then(data => this.childrenList = data);
			} else if (filter.filterTab === ETab.TYPESITES) {
				this.srv.siteSrv.getTypeSitesByFilter(filter).then(data => this.typeSitesList = data);
			} else if (filter.filterTab === ETab.HOUSEHOLDS) {
				this.srv.househSrv.getHouseholdsByFilter(filter).then(data => this.householdsList = data);
			}
		}
	};
	
  	@Output() onTabChanged = new EventEmitter();
  	@Output() onItemSelected = new EventEmitter();
  
    private childrenList: IChild[];
  	private typeSitesList: ITypeSite[];
	private householdsList: IHousehold[];
	
	constructor(private srv: ServiceManager) {
		this.srv.peopleSrv.getChildren().then(data => this.childrenList = data);
		this.srv.siteSrv.getTypeSitesTree().then(data => this.typeSitesList = data);
		this.srv.househSrv.getHouseholdsTree().then(data => this.householdsList = data);
	}
  
  	tabChanged(tab: number): void { 
		if (tab === 1) {
			this.onTabChanged.emit(ETab.CHILDREN);	
		} else if (tab === 2) {
			this.onTabChanged.emit(ETab.TYPESITES);
		} else if (tab === 3) {
			this.onTabChanged.emit(ETab.HOUSEHOLDS);
		}
	}

	onTypeSiteSelected(typeSite: ITypeSite): void { this.onItemSelected.emit(typeSite); }
	  
	onSiteSelected(site: ISite): void { this.onItemSelected.emit(site); }
	
	onGroupSelected(group: IGroup): void { this.onItemSelected.emit(group); }
	
	onHouseholdSelected(household: IHousehold): void { this.onItemSelected.emit(household); }
	
	onPersonSelected(person: IPerson): void { this.onItemSelected.emit(person); }
	
	onChildSelected(child: IChild): void { this.onItemSelected.emit(child); }
}