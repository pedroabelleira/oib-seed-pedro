import {Component, Input, Output, EventEmitter}	from 'angular2/core';
import {ISelectable, ESelectable}				from '../../api';
import {IChild}									from '../../services/person/person.interface';
import {DetailChildComponent}					from '../detail-child/detail-child.component';

@Component({
	selector: 'panel-detail',
  	templateUrl: 'apps/kiddyweb/components/panel-detail/template.html',
  	styleUrls: ['apps/kiddyweb/components/panel-detail/style.css'],
	directives: [DetailChildComponent]
})
export class PanelDetailComponent {
	@Input() item: ISelectable;
	
	@Output() onChildUpdated = new EventEmitter();
	
	constructor() {}
	
	/* Returns whether a div have to be shown according to the current item */
	private isDivDisplayed(item: ISelectable, type: ESelectable) {
		if (!item) return false;
		return (item.itemType === type);
	}
	
	/* Functions to decide whether a div is displayed or not */
	isParentDisplayed(): boolean { return this.isDivDisplayed(this.item, ESelectable.PARENT); }
	isChildDisplayed(): boolean { return this.isDivDisplayed(this.item, ESelectable.CHILD); }
	isHouseholdDisplayed(): boolean { return this.isDivDisplayed(this.item, ESelectable.HOUSEHOLD); }
	isTypesiteDisplayed(): boolean { return this.isDivDisplayed(this.item, ESelectable.TYPE_SITE); }
	isSiteDisplayed(): boolean { return this.isDivDisplayed(this.item, ESelectable.SITE); }
	isGroupDisplayed(): boolean { return this.isDivDisplayed(this.item, ESelectable.GROUP); }
	
	/* Responses to events */
	childUpdated(child: IChild): void {
		this.onChildUpdated.emit(child);
	}
	
}