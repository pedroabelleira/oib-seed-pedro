import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {IChild} from '../../services/person/person.interface'
import {ChildFeaturesComponent} from '../child-features/child-features.component';
import {ChildHouseholdComponent} from '../child-household/child-household.component';
import {ServiceManager} from '../../services/kw-service.service';

@Component({
	selector: 'detail-child',
  	templateUrl: 'apps/kiddyweb/components/detail-child/template.html',
  	styleUrls: ['apps/kiddyweb/components/detail-child/style.css'],
	directives: [ChildFeaturesComponent, ChildHouseholdComponent]
})
export class DetailChildComponent {
	@Input() child: IChild;
	
	@Output() onChildUpdated = new EventEmitter();

	constructor(private srv: ServiceManager) {}
	
	childUpdated(child: IChild): void {
		this.onChildUpdated.emit(child);
	}
	
}