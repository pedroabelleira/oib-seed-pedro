import {Component, Input, Output, EventEmitter}	from 'angular2/core';
import {IChild} from '../../services/person/person.interface';

@Component({
  selector: 'panel-list',
  templateUrl: 'apps/kiddyweb/components/panel-list/template.html',
  styleUrls: ['apps/kiddyweb/components/panel-list/style.css']
})
export class PanelListComponent {

  @Input() items: IChild[];
	@Output() onNodeSelected = new EventEmitter();
	
	onItemSelected(child: IChild): void { this.onNodeSelected.emit(child); }
  
}