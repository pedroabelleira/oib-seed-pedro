import {Component} from 'angular2/core';
import {EType, ETypeSuggestion, IFilter, ETab, ISelectable} from './api';
import {FilterDateComponent} from './components/filter-date/filter-date.component';
import {FilterNameComponent} from './components/filter-name/filter-name.component';
import {TabContainerComponent} from './components/tab-container/tab-container.component';
import {PanelDetailComponent} from './components/panel-detail/panel-detail.component';
import {IParent, IChild} from './services/person/person.interface';
import {ITypeSite, ISite, IGroup} from './services/site/site.interface';
import {IHousehold} from './services/household/household.interface';
import {ServiceManager} from './services/kw-service.service';


declare var _: any;

@Component({
    selector: 'oib-kw',
    templateUrl: 'apps/kiddyweb/app.html',
    styleUrls: ['apps/kiddyweb/app.css'],
    directives: [FilterDateComponent, FilterNameComponent, TabContainerComponent, PanelDetailComponent],
    providers: [ServiceManager]
})
export class AppComponent {
    // object storing the user filters
    private filter: IFilter = {
        filterType: EType.ALL,
        filterTab: ETab.CHILDREN
    };
    
    private applyFilter: IFilter;
        
    // Initial lists of elements
    private suggestionsType: ETypeSuggestion;
    
    // Currently selected item (to be removed ??)
    private selectedItem: ISelectable;
    
    
    constructor(private srv: ServiceManager) {
        this.suggestionsType = ETypeSuggestion.CHILD;
    }
        
    /* Store "type" when a new type has been clicked */
    typeChanged(type: EType): void { this.filter.filterType = type; }
    
    /* Store "date" when a new date has been selected */
    dateChanged(date: string): void { this.filter.filterDate = date; }
    
    /* Change autocomplete's dataset when a different tab has been activated */
    tabChanged(tab: ETab): void {
        this.filter.filterTab = tab;
        
        switch (tab) {
            case ETab.CHILDREN: this.suggestionsType = ETypeSuggestion.CHILD;
                break;
            case ETab.TYPESITES: this.suggestionsType = ETypeSuggestion.SITE;
                break;
            case ETab.HOUSEHOLDS: this.suggestionsType = ETypeSuggestion.HOUSEHOLD;
                break;            
        }
    }

    /* Display detail panel when an item has been selected */
    showDetail(item: ISelectable): void {
        this.selectedItem = item;
    }
            
    /* Filter elements when the "search" button was clicked */
    search(text: string): void {
        this.filter.filterText = text;
        this.applyFilter = _.defaults({}, this.filter);     // do it this way to force applyFilter being a new object every time. otherwise doesn't work
    }
    
    /* Display a pop-up to perform a detailed search (not impelmented yet) */
    searchAdv(text: string): void {
        this.filter.filterText = text;
    }
    
    /**
     * This action must be avoided but it's still required due to the events propagation system
     * implemented by angular2. The best approach is to fire an event only when the user confirms
     * the changes.
     */
    childUpdated(child: IChild):void {
        this.srv.peopleSrv.setChild(child);
    }
    
}