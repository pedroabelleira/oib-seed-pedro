export enum EType { ALL, FAVOURITES, DATE };

export enum ETypeSuggestion { CHILD, SITE, HOUSEHOLD };

export enum ETab { CHILDREN, TYPESITES, HOUSEHOLDS };

export enum ESelectable { PERSON, PARENT, CHILD, HOUSEHOLD, TYPE_SITE, SITE, GROUP };

export interface IType {
  id: EType;
  label: string;
  isActive: boolean;
}

export interface IFilter {
    filterType: EType,
    filterDate?: string,
    filterText?: string,
    filterTab: ETab
}

export interface ISelectable {
	name: string,
	itemType: ESelectable
}