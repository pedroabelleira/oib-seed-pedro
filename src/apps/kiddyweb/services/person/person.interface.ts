import {ISelectable}		from '../../api';

export interface IPerson extends ISelectable {
	id: number,
	name: string,
	birthDate?: string,
	perId?: string,
	firstName?: string,
	lastName?: string,
	gender?: string,
	typeDataSource?: any,
	contactData?: any[],
	nationality?: any,
	text?: string,
	icon?: string
}

export interface IParent extends IPerson {
	noSysper?: string,
	nup?: string,
	addressAdmin?: string,
	emailAdmin?: string,
	phoneAdmin?: string,
	addressPriv?: string,
	emailPriv?: string,
	phonePriv?: string,
	gsm?: string,
	maritalStatus?: any
}

export interface IChild extends IPerson {
	isBorn?: string,	
	school?: string,
	language?: string,
	payingParent?: IParent,
	secondParent?: IParent
}