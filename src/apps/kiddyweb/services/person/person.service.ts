import {Injectable, Inject} from 'angular2/core';
import {IChild}	from './person.interface';
import {CHILDREN} from '../../mocks/mock-people';
import {IFilter, EType} from '../../api';

@Injectable()
export class PeopleService {

	constructor() {}
	
	/**
	 * Returns the list of children's names.
	 */
	getSuggestionables(): Promise<any[]> {
		var result: any[] = [];
		var child: IChild;
		var i: number;
		
		for (i = 0; i < CHILDREN.length; i++) {
			child = CHILDREN[i];
			result.push({ name: child.name });
		}
		
		return Promise.resolve(result);
	}
	
	/**
	 * Returns the list of all the existing children.
	 */
	getChildren(): Promise<IChild[]> {
		return Promise.resolve(CHILDREN);
	}
		
	/**
	 * Return the child with the given id.
	 */
	getChildById(id: number): Promise<IChild> {
		var child: IChild;
		
		for (var i = 0; i < CHILDREN.length; i++) {
			child = CHILDREN[i];
			if (child.id === id) {
				return Promise.resolve(child);
			}	
		}
		
		return null;
	}
	
	/**
	 * Returns a list of children matching the given filter.
	 */
	getChildrenByFilter(filter: IFilter): Promise<IChild[]> {
		var result: IChild[] = [];
		
		result = CHILDREN.filter((child: IChild) => {
			var today: any = new Date();
			var birthDate: any = (child.birthDate ? this.parseDate(child.birthDate) : null);
			var fdate: any = (filter.filterDate ? this.parseDate(filter.filterDate) : null);

			if (filter.filterText.length > 0) {
				if (child.name.toLowerCase().indexOf(filter.filterText.toLowerCase()) < 0)
					return false;
			}
			if (filter.filterType === EType.DATE) {
				if ( child.isBorn === "0" || (child.isBorn === "1" && birthDate > fdate) )
					return false;
			} else if (filter.filterType === EType.FAVOURITES) {
				if ( child.isBorn === "0" || (child.isBorn === "1" && birthDate > today) )
					return false;
			}
			return true;
		});
		
		return Promise.resolve(result);
	}
	
	/**
	 * Applies modifications on a given child.
	 */
	setChild(child: IChild): void {
		var db_child: IChild;
		
		for (var i = 0; i < CHILDREN.length; i++) {
			db_child = CHILDREN[i];
			if (db_child.id === child.id) {
				db_child = child;
			}
		}
	}
	
    private parseDate(input: string): any {
        var parts: Array<string> = input.split('/');
        var day: number = Number.parseInt(parts[0]);
        var month: number = Number.parseInt(parts[1]) - 1;
        var year: number = Number.parseInt(parts[2]);

        return new Date(year, month, day); // Note: months are 0-based
    }
	
}