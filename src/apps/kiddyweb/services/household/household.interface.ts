import {ISelectable}		from '../../api';
import {IParent, IChild}	from '../person/person.interface';

export interface IHousehold extends ISelectable {
	householdId: number,
	startDate?: string,
	endDate?: string,
	payingParent?: IParent,
	secondParent?: IParent,
	children?: IChild[],
	parentNames?: string,
	icon?: string,			// ui
	text?: string,			// ui
	nodes?: IParent[]	// ui
}