import {Injectable, Inject}	from 'angular2/core';
import {IHousehold} from './household.interface';
import {IChild, IParent}	from '../person/person.interface';
import {HOUSEHOLDS} from '../../mocks/mock-household';
import {IFilter, EType} from '../../api';

@Injectable()
export class HouseholdService {
	
	constructor() {}
	
	/**
	 * Returns the list of all of members in the existing households as a tree.
	 */
	getHouseholdsTree(): Promise<IHousehold[]> {
		var result: IHousehold[] = [];
		var household: IHousehold;
		var child: IChild;
		var i, j: number;

		for (i = 0; i < HOUSEHOLDS.length; i++) {
			household = HOUSEHOLDS[i];			
			household.text = household.parentNames;
			household.icon = "glyphicon glyphicon-home";
			household.nodes = [];
			
			if (household.payingParent) {
				household.payingParent.text = household.payingParent.name;
				household.payingParent.icon = "glyphicon glyphicon-user";
				household.nodes.push(household.payingParent);
			}
			if (household.secondParent) {
				household.secondParent.text = household.secondParent.name;
				household.secondParent.icon = "glyphicon glyphicon-tags";
				household.nodes.push(household.secondParent);	
			}
			if (household.children && household.children.length > 0) {
				for (j = 0; j < household.children.length; j++) {
					child = household.children[j];
					child.text = child.name;
					child.icon = "glyphicon glyphicon-blackboard";
					household.nodes.push(child);
				}
			}

			result.push(household);
		}
		
		return Promise.resolve(result);
	}
	
	/**
	 * Returns the list of all the existing households.
	 */
	getHouseholds(): Promise<IHousehold[]> {
		return Promise.resolve(HOUSEHOLDS);
	}

	/**
	 * Returns the list of all the members in the existing households (children and parents).
	 */
	getSuggestionables(): Promise<any[]> {
		var result: any[] = [];
		var household: IHousehold;
		var i, j: number;
		
		for (i = 0; i < HOUSEHOLDS.length; i++) {
			household = HOUSEHOLDS[i];
			result.push({ name: household.parentNames });
			if (household.payingParent) {
				result.push({ name: household.payingParent.name });
			}
			if (household.secondParent) {
				result.push({ name: household.secondParent.name });
			}
			if (household.children && household.children.length > 0) {
				for (j = 0; j < household.children.length; j++) {
					result.push({ name: household.children[j].name });
				}
			}
		}
		
		return Promise.resolve(result);
	}

	// TODO improve it and implement filter by date
	getHouseholdsByFilter(filter: IFilter): Promise<IHousehold[]> {
		var result: IHousehold[] = [];

		result = HOUSEHOLDS.filter((household: IHousehold) => {
			if (filter.filterText.length <= 0) {
				return true;
			}
			
			var pattern: string = filter.filterText.toLowerCase();
			
			if (household.name.toLowerCase().indexOf(pattern) < 0) {
				var pp: IParent = household.payingParent;					
				var p2: IParent = household.secondParent;
				var ch: IChild[] = household.children;
				
				if (pp) {
					if (pp.name.toLowerCase().indexOf(pattern) > 0)
						return true;
				}
				if (p2) {
					if (pp.name.toLowerCase().indexOf(pattern) > 0)
						return true;
				}
				if (ch && ch.length > 0) {
					for (var i = 0; i < ch.length; i++) {
						var child: IChild = ch[i];
						if (child.name.toLowerCase().indexOf(pattern) > 0)
							return true;
					}	
				}
				return false;
			}
			
			return true;
		});	
		
		return Promise.resolve(result);
	}
	
}