import {Injectable} from 'angular2/core';

import {HouseholdService}	from './household/household.service';
import {PeopleService} 		from './person/person.service';
import {SitesService} 		from './site/site.service';

@Injectable()
export class ServiceManager {
	
	constructor(
		public househSrv: HouseholdService,
		public peopleSrv: PeopleService,
		public siteSrv: SitesService
	) {}

    // Transform a string into a date object
    public parseDate(input: string): any {
        var parts: Array<string> = input.split('/');
        var day: number = Number.parseInt(parts[0]);
        var month: number = Number.parseInt(parts[1]) - 1;
        var year: number = Number.parseInt(parts[2]);

        return new Date(year, month, day); // Note: months are 0-based
    }
	
}
