import {ISelectable} from '../../api';

export interface ITypeSite extends ISelectable {
	typeSiteId: number, 
	name: string,
	sites?: ISite[],
	text?: string,		// ui	
	nodes?: ISite[]		// ui
}

export interface ISite extends ISelectable {
	siteId: number,
	name: string,
	code: string,
	shortName?: string,
	comments?: string,
	year?: string,
	dtDebut: string,
	dtFin: string,
	typeSite: ITypeSite,
	groups?: IGroup[],
	text?: string,		// ui
	nodes?: IGroup[]	// ui
}

export interface IGroup extends ISelectable {
	groupeId: number,
	addressAdm?: string,
	capacity?: number,
	name: string,
	phone?: string,
	dtDebut: string,
	dtFin: string,
	site: ISite,
	text?: string		// ui	
}