import {Injectable, Inject} from 'angular2/core';
import {ISite, ITypeSite, IGroup} from './site.interface';
import {GROUPS, SITES, TYPESITES} from '../../mocks/mock-sites';
import {IFilter, EType} from '../../api';

@Injectable()
export class SitesService {
	
	constructor() { }
	
	/**
	 * Returns the list of all of type of sites, sites and groups as a tree.
	 */
	getTypeSitesTree(): Promise<ITypeSite[]> {
		var result: ITypeSite[] = [];
		var typeSite: ITypeSite;
		var site: ISite;
		var group: IGroup;
		var i, j, k: number;
		
		for (i = 0; i < TYPESITES.length; i++) {
			typeSite = TYPESITES[i];
			typeSite.text = typeSite.name;
			typeSite.nodes = [];
			
			for (j = 0; j < SITES.length; j++) {
				site = SITES[j];
				if (site.typeSite.typeSiteId === typeSite.typeSiteId) {
					site.text = site.name;
					site.nodes = [];
					for (k = 0; k < GROUPS.length; k++) {
						group = GROUPS[k];
						if (group.site.siteId === site.siteId) {
							group.text = group.name;
							site.nodes.push(group);	
						}
					}
					typeSite.nodes.push(site);
				}
			}

			result.push(typeSite);
		}
		
		return Promise.resolve(result);
	}
	
	/**
	 * Returns the list of all the existing type of sites.
	 */
	getTypeSites(): Promise<ITypeSite[]> {
		var result: ITypeSite[] = [];
		var i: number = 0;
		var typeSite: ITypeSite;
		
		for (i = 0; i < TYPESITES.length; i++) {
			typeSite = TYPESITES[i];
			typeSite.text = typeSite.name;
			result.push(typeSite);
		}
		
		return Promise.resolve(result);
	}
	
	/**
	 * Returns the list of all the existing sites.
	 */
	getSites(): Promise<ISite[]> {
		var result: ISite[] = [];
		var i: number = 0;
		var site: ISite;
		
		for (i = 0; i < SITES.length; i++) {
			site = SITES[i];
			site.text = site.name;
			result.push(site);
		}
		
		return Promise.resolve(result);
	}

	/**
	 * Returns the list of all the existing type sites, sites and groups.
	 */
	getSuggestionables(): Promise<any[]> {
		var result: any[] = [];

		result.push.apply(result, TYPESITES);
		result.push.apply(result, SITES);
		result.push.apply(result, GROUPS);
		return Promise.resolve(result);
	}
	
	// TODO improve it and implement filter by date
	getTypeSitesByFilter(filter: IFilter): Promise<ITypeSite[]> {
		var result: ITypeSite[] = [];
		
		result = TYPESITES.filter((typeSite: ITypeSite) => {
			if (filter.filterText.length > 0) {
				var pattern: string = filter.filterText.toLowerCase();
				var found: boolean = false;
				
				if (typeSite.name.toLowerCase().indexOf(pattern) < 0) {
					var sites: ISite[] = typeSite.nodes;
					for (var i = 0; i < sites.length; i++) {
						var site: ISite = sites[i];
						if (site.name.toLowerCase().indexOf(pattern) < 0) {
							var groups: IGroup[] = site.nodes;
							for (var j = 0; j < groups.length; j++) {
								var group: IGroup = groups[j];
								if (group.name.toLowerCase().indexOf(pattern) >= 0) {
									found = true;
								}
							}
						} else {
							found = true;
						}
					}
				} else {
					found = true;
				}
			} else {
				found = true;
			}
			
			return found;
		});
		
		return Promise.resolve(result);
	}
	
}
