import {IChild} from '../services/person/person.interface';
import {ESelectable} from '../api';

export var CHILDREN: IChild[] = [{
	id: 1,
	name: "AGNEESSENS Charlène",
	lastName: "AGNEESSENS",
	firstName: "Charlène",
	isBorn: "1",
	gender: "F",
	birthDate: "01/05/2016",
	itemType: ESelectable.CHILD,
	payingParent: {
		id: 20000,
		name: "VAN PELT, CAROLINE",
		firstName: "CAROLINE",
		lastName: "VAN PELT",
		itemType: ESelectable.PARENT,
		gender: "F",
		perId: "000123456"
	},
	secondParent: {
		id: 30000,
		name: "VAN HOUTE, Benoit",
		firstName: "Benoit",
		lastName: "VAN HOUTE",
		itemType: ESelectable.PARENT,
		gender: "F",
		perId: "001234567"
	}
}, {
	id: 2,
	name: "AGNEESSENS Grégoire",
	lastName: "AGNEESSENS",
	firstName: "Grégoire",
	isBorn: "1",
	gender: "M",
	birthDate: "02/04/2014",
	itemType: ESelectable.CHILD,
	payingParent: {
		id: 20001,
		name: "LEHTVEER, Merle",
		firstName: "Merle",
		lastName: "LEHTVEER",
		itemType: ESelectable.PARENT,
		gender: "F",
		perId: "012345678"
	},
	secondParent: {
		id: 30001,
		name: "SANTANGELO, Alfio",
		firstName: "Alfio",
		lastName: "SANTANGELO",
		itemType: ESelectable.PARENT,
		gender: "M",
		perId: "123456789"
	}
}, {
	id: 3,
	name: "AGNEESSENS Lydia",
	lastName: "AGNEESSENS",
	firstName: "Lydia",
	isBorn: "0",
	gender: "F",
	itemType: ESelectable.CHILD,
	payingParent: {
		id: 20003,
		name: "O'NEILL, EOGHAN",
		firstName: "EOGHAN",
		lastName: "O'NEILL",
		itemType: ESelectable.PARENT,
		gender: "M",
		perId: "456789012"
	}
}, {
	id: 4,
	name: "AGOSTINI Claude",
	lastName: "AGOSTINI",
	firstName: "Claude",
	isBorn: "0",
	gender: "M",
	itemType: ESelectable.CHILD,
	payingParent: {
		id: 20002,
		name: "EBLING, Guenther",
		firstName: "Guenther",
		lastName: "EBLING",
		itemType: ESelectable.PARENT,
		gender: "M",
		perId: "234567890"
	},
	secondParent: {
		id: 30002,
		name: "EBLING, Bianka",
		firstName: "Bianka",
		lastName: "EBLING",
		itemType: ESelectable.PARENT,
		gender: "F",
		perId: "345678901"
	}
}, {
	id: 5,
	name: "AGOSTINI Francesca",
	lastName: "AGOSTINI",
	firstName: "Francesca",
	isBorn: "1",
	gender: "F",
	birthDate: "03/03/2015",
	itemType: ESelectable.CHILD
}, {
	id: 6,
	name: "AGOSTINI Raphael",
	lastName: "AGOSTINI",
	firstName: "Raphael",
	isBorn: "1",
	gender: "M",
	birthDate: "04/02/2011",
	itemType: ESelectable.CHILD
}, {
	id: 7,
	name: "AGOSTINI Samuel",
	lastName: "AGOSTINI",
	firstName: "Samuel",
	isBorn: "1",
	gender: "M",
	birthDate: "05/01/2010",
	itemType: ESelectable.CHILD
}, {
	id: 8,
	name: "AGRA CHAVES Ana-Maria",
	lastName: "AGRA CHAVES",
	firstName: "Ana-Maria",
	isBorn: "1",
	gender: "F",
	birthDate: "06/12/2010",
	itemType: ESelectable.CHILD
}, {
	id: 9,
	name: "AGUERO Suzanne",
	lastName: "AGUERO",
	firstName: "Suzanne",
	isBorn: "0",
	gender: "F",
	itemType: ESelectable.CHILD
}, {
	id: 10,
	name: "AGUIAR BOFARULL Anna",
	lastName: "AGUIAR BOFARULL",
	firstName: "Anna",
	isBorn: "1",
	gender: "F",
	birthDate: "07/11/2010",
	itemType: ESelectable.CHILD
}, {
	id: 11,
	name: "AGUIAR LOPEZ Juan",
	lastName: "AGUIAR LOPEZ",
	firstName: "Juan",
	isBorn: "1",
	gender: "M",
	birthDate: "08/10/2011",
	itemType: ESelectable.CHILD
}, {
	id: 12,
	name: "AGUILERA Juana",
	lastName: "AGUILERA",
	firstName: "Juana",
	isBorn: "1",
	gender: "F",
	birthDate: "09/09/2013",
	itemType: ESelectable.CHILD
}, {
	id: 13,
	name: "AGUIRRE Lydia",
	lastName: "AGUIRRE",
	firstName: "Lydia",
	isBorn: "1",
	gender: "F",
	birthDate: "10/08/2012",
	itemType: ESelectable.CHILD
}, {
	id: 14,
	name: "AGUSTIN Ramon Labernia",
	lastName: "AGUSTIN",
	firstName: "Ramon Labernia",
	isBorn: "1",
	gender: "M",
	birthDate: "11/07/2013",
	itemType: ESelectable.CHILD
}, {
	id: 15,
	name: "AGWU Emmanuel Onuoha",
	lastName: "AGWU",
	firstName: "Emmanuel Onuoha",
	isBorn: "1",
	gender: "M",
	birthDate: "12/06/2013",
	itemType: ESelectable.CHILD
}, {
	id: 16,
	name: "AHAJJAM Rachid",
	lastName: "AHAJJAM",
	firstName: "Rachid",
	isBorn: "1",
	gender: "M",
	birthDate: "13/05/2013",
	itemType: ESelectable.CHILD
}, {
	id: 17,
	name: "AHAJJAM-VILUGRON Shirin",
	lastName: "AHAJJAM-VILUGRON",
	firstName: "Shirin",
	isBorn: "1",
	gender: "M",
	birthDate: "14/04/2012",
	itemType: ESelectable.CHILD
}, {
	id: 18,
	name: "AHKIM Rabia",
	lastName: "AHKIM",
	firstName: "Rabia",
	isBorn: "1",
	gender: "F",
	birthDate: "15/03/2012",
	itemType: ESelectable.CHILD
}, {
	id: 19,
	name: "AHLBERG Anders",
	lastName: "AHLBERG",
	firstName: "Anders",
	isBorn: "0",
	gender: "M",
	itemType: ESelectable.CHILD
}, {
	id: 20,
	name: "AHLBERG Filip",
	lastName: "AHLBERG",
	firstName: "Filip",
	isBorn: "1",
	gender: "M",
	birthDate: "16/02/2014",
	itemType: ESelectable.CHILD
}, {
	id: 21,
	name: "AHLBERG Malgorzata",
	lastName: "AHLBERG",
	firstName: "Malgorzata",
	isBorn: "1",
	gender: "F",
	birthDate: "17/01/2016",
	itemType: ESelectable.CHILD
}, {
	id: 22,
	name: "AHLBERG Nina",
	lastName: "AHLBERG",
	firstName: "Nina",
	isBorn: "1",
	gender: "F",
	birthDate: "18/12/2015",
	itemType: ESelectable.CHILD
}, {
	id: 23,
	name: "AHLBERG Sebastian",
	lastName: "AHLBERG",
	firstName: "Sebastian",
	isBorn: "1",
	gender: "M",
	birthDate: "19/11/2011",
	itemType: ESelectable.CHILD
}, {
	id: 24,
	name: "AHLBRECHT Anna Amalia",
	lastName: "AHLBRECHT",
	firstName: "Anna Amalia",
	isBorn: "1",
	gender: "F",
	birthDate: "20/10/2010",
	itemType: ESelectable.CHILD
}, {
	id: 25,
	name: "AHLBRECHT Gerhard",
	lastName: "AHLBRECHT",
	firstName: "Gerhard",
	isBorn: "1",
	gender: "M",
	birthDate: "21/09/2009",
	itemType: ESelectable.CHILD
}, {
	id: 26,
	name: "AHLBRECHT Sonia",
	lastName: "AHLBRECHT",
	firstName: "Sonia",
	isBorn: "0",
	gender: "F",
	itemType: ESelectable.CHILD
}, {
	id: 27,
	name: "AHLBRECHT Victoria Luise",
	lastName: "AHLBRECHT",
	firstName: "Victoria Luise",
	isBorn: "1",
	gender: "F",
	birthDate: "22/08/2010",
	itemType: ESelectable.CHILD
}, {
	id: 28,
	name: "AHLROTH Jari",
	lastName: "AHLROTH",
	firstName: "Jari",
	isBorn: "1",
	gender: "M",
	birthDate: "23/07/2011",
	itemType: ESelectable.CHILD
}, {
	id: 29,
	name: "AHRENKILDE HANSEN Pia",
	lastName: "AHRENKILDE HANSEN",
	firstName: "Pia",
	isBorn: "1",
	gender: "F",
	birthDate: "24/06/2012",
	itemType: ESelectable.CHILD
}, {
	id: 30,
	name: "AHRENS Birgit",
	lastName: "AHRENS",
	firstName: "Birgit",
	isBorn: "1",
	gender: "F",
	birthDate: "25/05/2013",
	itemType: ESelectable.CHILD
}, {
	id: 31,
	name: "AHRIPOU Mohamed",
	lastName: "AHRIPOU",
	firstName: "Mohamed",
	isBorn: "0",
	gender: "M",
	itemType: ESelectable.CHILD
}, {
	id: 32,
	name: "AHRIPOU Zachari",
	lastName: "AHRIPOU",
	firstName: "Zachari",
	isBorn: "1",
	gender: "M",
	birthDate: "26/04/2014",
	itemType: ESelectable.CHILD
}, {
	id: 33,
	name: "AHYATEN Mohammed Younes",
	lastName: "AHYATEN",
	firstName: "Mohammed Younes",
	isBorn: "1",
	gender: "M",
	birthDate: "27/03/2013",
	itemType: ESelectable.CHILD
}, {
	id: 34,
	name: "AHYATEN Walid",
	lastName: "AHYATEN",
	firstName: "Walid",
	isBorn: "1",
	gender: "M",
	birthDate: "28/02/2011",
	itemType: ESelectable.CHILD
}, {
	id: 35,
	name: "AIGROT Ludovic",
	lastName: "AIGROT",
	firstName: "Ludovic",
	isBorn: "0",
	gender: "M",
	itemType: ESelectable.CHILD
}, {
	id: 36,
	name: "ALABISO Sergio",
	lastName: "ALABISO",
	firstName: "Sergio",
	isBorn: "1",
	gender: "M",
	birthDate: "29/01/2012",
	itemType: ESelectable.CHILD
}];