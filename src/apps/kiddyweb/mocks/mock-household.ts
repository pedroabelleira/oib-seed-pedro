import {IHousehold}	from '../services/household/household.interface';
import {ESelectable} from '../api';

export var HOUSEHOLDS: IHousehold[] = [{
	householdId: 11563,
	startDate: "01/01/2007",
	endDate: "01/01/9999",
	parentNames: "VAN PELT-VAN HOUTE",
	name: "VAN PELT-VAN HOUTE",
	payingParent: {
		id: 20000,
		name: "VAN PELT, CAROLINE",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30000,
		name: "VAN HOUTE, Benoit",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 14834,
	startDate: "01/02/2009",
	endDate: "01/01/9999",
	parentNames: "LEHTVEER-SANTANGELO",
	name: "LEHTVEER-SANTANGELO",
	payingParent: {
		id: 20001,
		name: "LEHTVEER, Merle",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30001,
		name: "SANTANGELO, Alfio",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 15098,
	startDate: "25/03/2008",
	endDate: "01/01/9999",
	parentNames: "EBLING-EBLING",
	name: "EBLING-EBLING",
	payingParent: {
		id: 20002,
		name: "EBLING, Guenther",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30002,
		name: "EBLING, Bianka",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 15383,
	startDate: "01/09/2007",
	endDate: "01/01/9999",
	parentNames: "O'NEILL-O'NEILL",
	name: "O'NEILL-O'NEILL",
	payingParent: {
		id: 20003,
		name: "O'NEILL, EOGHAN",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30003,
		name: "O'NEILL, CAITRIONA",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 16826,
	startDate: "01/06/2010",
	endDate: "01/01/9999",
	parentNames: "BOTSOVA-BOCHEV",
	name: "BOTSOVA-BOCHEV",
	payingParent: {
		id: 20004,
		name: "BOTSOVA, DARINA",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30004,
		name: "BOCHEV, Ivan",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 16838,
	startDate: "01/08/2010",
	endDate: "01/01/9999",
	parentNames: "PESTA-PESTA",
	name: "PESTA-PESTA",
	payingParent: {
		id: 20005,
		name: "PESTA, Michal",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30005,
		name: "PESTA, Silvena",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 16922,
	startDate: "01/06/2009",
	endDate: "01/01/9999",
	parentNames: "WALTER-SCHROTH",
	name: "WALTER-SCHROTH",
	payingParent: {
		id: 20006,
		name: "WALTER, Egbert",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30006,
		name: "SCHROTH, Susanne",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 16934,
	startDate: "01/07/2009",
	endDate: "01/01/9999",
	parentNames: "KRZEMINSKA-VAMVAKA-VAMVAKAS",
	name: "KRZEMINSKA-VAMVAKA-VAMVAKAS",
	payingParent: {
		id: 20007,
		name: "KRZEMINSKA-VAMVAKA, Joanna",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30007,
		name: "VAMVAKAS, Vasileios",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17235,
	startDate: "01/06/2010",
	endDate: "01/01/9999",
	parentNames: "COSCO-LUISE",
	name: "COSCO-LUISE",
	payingParent: {
		id: 20008,
		name: "COSCO, Marianna",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30008,
		name: "LUISE, Raffaele",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17291,
	startDate: "01/01/2011",
	endDate: "01/01/9999",
	parentNames: "HEERES-GAJDA",
	name: "HEERES-GAJDA",
	payingParent: {
		id: 20009,
		name: "HEERES, Arnoud",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30009,
		name: "GAJDA, Aneta",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17302,
	startDate: "01/02/2011",
	endDate: "01/01/9999",
	parentNames: "ZIOLKOWSKI-KRASICKA",
	name: "ZIOLKOWSKI-KRASICKA",
	payingParent: {
		id: 20010,
		name: "ZIOLKOWSKI, Jan",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30010,
		name: "KRASICKA, Agnieszka",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17321,
	startDate: "01/02/2011",
	endDate: "01/01/9999",
	parentNames: "JUVYNS-TRAN",
	name: "JUVYNS-TRAN",
	payingParent: {
		id: 20011,
		name: "JUVYNS, OLIVIER",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30011,
		name: "TRAN, Thi Chau Sa",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17350,
	startDate: "01/03/2011",
	endDate: "01/01/9999",
	parentNames: "GIORGIO-MOUDAKKIRE",
	name: "GIORGIO-MOUDAKKIRE",
	payingParent: {
		id: 20012,
		name: "GIORGIO, Alicia",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30012,
		name: "MOUDAKKIRE, Ridouane",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17353,
	startDate: "01/03/2011",
	endDate: "01/01/9999",
	parentNames: "KOZLOVA-KOZLOVS",
	name: "KOZLOVA-KOZLOVS",
	payingParent: {
		id: 20013,
		name: "KOZLOVA, Kristine",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30013,
		name: "KOZLOVS, Mihails",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17426,
	startDate: "01/05/2011",
	endDate: "01/01/9999",
	parentNames: "HANGYA-LACHMANN-HANGYA-LACHMANN",
	name: "HANGYA-LACHMANN-HANGYA-LACHMANN",
	payingParent: {
		id: 20014,
		name: "HANGYA-LACHMANN, GABOR",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30014,
		name: "HANGYA-LACHMANN, Noemi",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17465,
	startDate: "01/06/2011",
	endDate: "01/01/9999",
	parentNames: "LAPKOWSKA - ZARZYCKA-ZARZYCKI",
	name: "LAPKOWSKA - ZARZYCKA-ZARZYCKI",
	payingParent: {
		id: 20015,
		name: "LAPKOWSKA - ZARZYCKA, ELZBIETA",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30015,
		name: "ZARZYCKI, Marcin",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17467,
	startDate: "01/06/2011",
	endDate: "01/01/9999",
	parentNames: "NESHEVA-PANTEV",
	name: "NESHEVA-PANTEV",
	payingParent: {
		id: 20016,
		name: "NESHEVA, LYUBOMIRA",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30016,
		name: "PANTEV, Assen",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17469,
	startDate: "01/06/2011",
	endDate: "01/01/9999",
	parentNames: "HABIAK-HABIAK",
	name: "HABIAK-HABIAK",
	payingParent: {
		id: 20017,
		name: "HABIAK, Lukasz",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30017,
		name: "HABIAK, Katarzyna",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17560,
	startDate: "01/05/2011",
	endDate: "01/01/9999",
	parentNames: "CHITESCU-AMBROZIE",
	name: "CHITESCU-AMBROZIE",
	payingParent: {
		id: 20018,
		name: "CHITESCU, FLORINA OANA",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30018,
		name: "AMBROZIE, Bogdan",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17621,
	startDate: "01/07/2011",
	endDate: "01/01/9999",
	parentNames: "WICZEWSKA-WICZEWSKI",
	name: "WICZEWSKA-WICZEWSKI",
	payingParent: {
		id: 20019,
		name: "WICZEWSKA, KATARZYNA ANNA",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30019,
		name: "WICZEWSKI, Bartlomiej",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}, {
	householdId: 17624,
	startDate: "01/07/2011",
	endDate: "01/01/9999",
	parentNames: "STUMPF-MOCKUTE-STUMPF",
	name: "STUMPF-MOCKUTE-STUMPF",
	payingParent: {
		id: 20020,
		name: "STUMPF, Ulrich",
		itemType: ESelectable.PARENT
	},
	secondParent: {
		id: 30020,
		name: "MOCKUTE-STUMPF, RUTA",
		itemType: ESelectable.PARENT
	},
	children: [],
	itemType: ESelectable.HOUSEHOLD
}];