import {ISite, ITypeSite, IGroup} from '../services/site/site.interface';
import {ESelectable} from '../api';

export var TYPESITES: ITypeSite[] = [{
	typeSiteId: 1,
	name: "Crèche",
	itemType: ESelectable.TYPE_SITE
}, {
	typeSiteId: 2,
	name: "Garderie",
	itemType: ESelectable.TYPE_SITE
}, {
	typeSiteId: 3,
	name: "Garderie aérée",
	itemType: ESelectable.TYPE_SITE
}];

export var SITES: ISite[] = [{
	siteId: 28,
	code: "CR BPARK",
	name: "Crèche BARNEPARK",
	shortName: "CR BARNEPARK",
	comments: "Mme Magali JESPERS - 02/296.94.61",
	year: null,
	dtDebut: "01/09/2011",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 9,
	code: "CR BEAU",
	name: "Crèche BEAULIEU",
	shortName: "CR BEAULIEU",
	comments: "H. Messalkhere (02/298 84 59)",
	year: null,
	dtDebut: "01/09/2001",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 10,
	code: "CR CLOVIS",
	name: "Crèche CLOVIS",
	shortName: "CR CLOVIS",
	comments: "Axel MERLE (02.29) 57553",
	year: null,
	dtDebut: "01/09/2001",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 26,
	code: "CR COLE",
	name: "Crèche COLE",
	shortName: "CR COLE",
	comments: "Mme Magali JESPERS - 02/296 94 61",
	year: null,
	dtDebut: "01/11/2010",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 20,
	code: "CR CONS",
	name: "Crèche CONSEIL",
	shortName: "CR CONSEIL",
	comments: "G. Cammarata (02-29) 95.664",
	year: null,
	dtDebut: "15/10/2006",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 2,
	code: "CR CORTEN",
	name: "Crèche CORTENBERGH",
	shortName: "CR CORTENBERGH",
	comments: "Fanny",
	year: null,
	dtDebut: "01/01/2012",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 1,
	code: "CR FLUTEXX",
	name: "Crèche FLUTE ENCHANTEE",
	shortName: "0.6913492",
	comments: "M. Caroes (02/2957264)",
	year: null,
	dtDebut: "30/06/2012",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 24,
	code: "CR FRANKL",
	name: "Crèche FRANKLIN",
	shortName: "CR FRANKLIN",
	comments: "Mme P. Caroes  (02/295.72.64)",
	year: null,
	dtDebut: "01/06/2006",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 16,
	code: "CR FROISS",
	name: "Crèche FROISSART",
	shortName: "CR FROISSART",
	comments: "M. Grobovenko (022963791)",
	year: null,
	dtDebut: "01/06/2006",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 22,
	code: "CR GENEVE",
	name: "Crèche GENEVE",
	shortName: "CR GENEVE",
	comments: "Grobovenko Nadia (02/296.37.91)",
	year: null,
	dtDebut: "01/09/2007",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 19,
	code: "CR GRANGE",
	name: "Crèche GRANGE",
	shortName: "CR GRANGE",
	comments: "A-M. Lopez (02 / 298 19 04) ",
	year: null,
	dtDebut: "01/03/2006",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 25,
	code: "CR M LOU",
	name: "Crèche MARIE LOUISE",
	shortName: "CR MARIE LOUISE",
	comments: "P.Caroes (02/2957264)",
	year: null,
	dtDebut: "01/12/2010",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 11,
	code: "CR PALM",
	name: "Crèche PALMERSTON",
	shortName: "CR PALMERSTON",
	comments: "G. Cammarata (02-29) 95.664",
	year: null,
	dtDebut: "19/09/2001",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 14,
	code: "CR TREFLE",
	name: "Crèche TREFLE 4 FEUILLES",
	shortName: "CR TREFLE 4F",
	comments: "H. Messalkhere (02/298 84 59)",
	year: null,
	dtDebut: "01/05/2004",
	dtFin: "31/01/2007",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 4,
	code: "CR WAYENB",
	name: "Crèche WAYENBERG",
	shortName: "CR WAYENBERG",
	comments: "A-M. Lopez (02 / 298 19 04) ",
	year: null,
	dtDebut: "01/01/2007",
	dtFin: "31/08/2008",
	typeSite: {
		typeSiteId: 1,
		name: "Crèche",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}, {
	siteId: 21,
	code: "EE BERKEND",
	name: "Ecole E BERKENDAEL",
	shortName: "EE Berkendael",
	comments: "G.Cammarata (02/2995664)",
	year: null,
	dtDebut: "01/08/2007",
	dtFin: "01/01/9999",
	typeSite: {
		typeSiteId: 2,
		name: "Garderie",
		itemType: ESelectable.TYPE_SITE
	},
	itemType: ESelectable.SITE
}];

export var GROUPS: IGroup[] = [{
	groupeId: 4,
	addressAdm: null,
	capacity: null,
	name: "FLUTE ENCHANTEE - Tous niveaux",
	phone: null,
	dtDebut: "30/07/2012",
	dtFin: "01/01/9999",
	site: {
		siteId: 1,
		code: "CR FLUTEXX",
		name: "Crèche FLUTE ENCHANTEE",
		shortName: "0.6913492",
		comments: "M. Caroes (02/2957264)",
		year: null,
		dtDebut: "30/06/2012",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 52,
	addressAdm: null,
	capacity: null,
	name: "Enfants Commission",
	phone: null,
	dtDebut: "01/01/2007",
	dtFin: "31/08/2008",
	site: {
		siteId: 4,
		code: "CR WAYENB",
		name: "Crèche WAYENBERG",
		shortName: "CR WAYENBERG",
		comments: "A-M. Lopez (02 / 298 19 04) ",
		year: null,
		dtDebut: "01/01/2007",
		dtFin: "31/08/2008",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 331,
	addressAdm: null,
	capacity: null,
	name: "SAVANNAH",
	phone: null,
	dtDebut: "01/10/2010",
	dtFin: "01/01/9999",
	site: {
		siteId: 10,
		code: "CR CLOVIS",
		name: "Crèche CLOVIS",
		shortName: "CR CLOVIS",
		comments: "Axel MERLE (02.29) 57553",
		year: null,
		dtDebut: "01/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 280,
	addressAdm: null,
	capacity: null,
	name: "Steve Van der Biest",
	phone: null,
	dtDebut: "17/08/2010",
	dtFin: "01/08/2012",
	site: {
		siteId: 21,
		code: "EE BERKEND",
		name: "Ecole E BERKENDAEL",
		shortName: "EE Berkendael",
		comments: "G.Cammarata (02/2995664)",
		year: null,
		dtDebut: "01/08/2007",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 2,
			name: "Garderie",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 310,
	addressAdm: null,
	capacity: null,
	name: "Pauline ",
	phone: null,
	dtDebut: "22/08/2012",
	dtFin: "01/01/9999",
	site: {
		siteId: 21,
		code: "EE BERKEND",
		name: "Ecole E BERKENDAEL",
		shortName: "EE Berkendael",
		comments: "G.Cammarata (02/2995664)",
		year: null,
		dtDebut: "01/08/2007",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 2,
			name: "Garderie",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 332,
	addressAdm: null,
	capacity: null,
	name: "Steve V.B. ",
	phone: null,
	dtDebut: "30/07/2013",
	dtFin: "01/01/9999",
	site: {
		siteId: 21,
		code: "EE BERKEND",
		name: "Ecole E BERKENDAEL",
		shortName: "EE Berkendael",
		comments: "G.Cammarata (02/2995664)",
		year: null,
		dtDebut: "01/08/2007",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 2,
			name: "Garderie",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 333,
	addressAdm: null,
	capacity: null,
	name: "Aïcha U.K. ",
	phone: null,
	dtDebut: "30/07/2013",
	dtFin: "01/01/9999",
	site: {
		siteId: 21,
		code: "EE BERKEND",
		name: "Ecole E BERKENDAEL",
		shortName: "EE Berkendael",
		comments: "G.Cammarata (02/2995664)",
		year: null,
		dtDebut: "01/08/2007",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 2,
			name: "Garderie",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 27,
	addressAdm: null,
	capacity: 12,
	name: "OPALE",
	phone: null,
	dtDebut: "01/09/2011",
	dtFin: "01/01/9999",
	site: {
		siteId: 9,
		code: "CR BEAU",
		name: "Crèche BEAULIEU",
		shortName: "CR BEAULIEU",
		comments: "H. Messalkhere (02/298 84 59)",
		year: null,
		dtDebut: "01/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 113,
	addressAdm: null,
	capacity: 15,
	name: "JADE",
	phone: "93587",
	dtDebut: "01/09/2000",
	dtFin: "01/01/9999",
	site: {
		siteId: 9,
		code: "CR BEAU",
		name: "Crèche BEAULIEU",
		shortName: "CR BEAULIEU",
		comments: "H. Messalkhere (02/298 84 59)",
		year: null,
		dtDebut: "01/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 167,
	addressAdm: null,
	capacity: null,
	name: "TURQUOISE",
	phone: null,
	dtDebut: "01/10/2004",
	dtFin: "01/01/9999",
	site: {
		siteId: 9,
		code: "CR BEAU",
		name: "Crèche BEAULIEU",
		shortName: "CR BEAULIEU",
		comments: "H. Messalkhere (02/298 84 59)",
		year: null,
		dtDebut: "01/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 124,
	addressAdm: null,
	capacity: 18,
	name: "ARTISTES",
	phone: "57782",
	dtDebut: "01/01/2000",
	dtFin: "01/01/9999",
	site: {
		siteId: 9,
		code: "CR BEAU",
		name: "Crèche BEAULIEU",
		shortName: "CR BEAULIEU",
		comments: "H. Messalkhere (02/298 84 59)",
		year: null,
		dtDebut: "01/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 359,
	addressAdm: null,
	capacity: 12,
	name: "BEGONIA",
	phone: null,
	dtDebut: "01/04/2014",
	dtFin: "01/01/9999",
	site: {
		siteId: 10,
		code: "CR CLOVIS",
		name: "Crèche CLOVIS",
		shortName: "CR CLOVIS",
		comments: "Axel MERLE (02.29) 57553",
		year: null,
		dtDebut: "01/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 68,
	addressAdm: "PALM 5P/20",
	capacity: 12,
	name: "VOIE LACTEE",
	phone: "65125",
	dtDebut: "10/09/2001",
	dtFin: "01/01/9999",
	site: {
		siteId: 11,
		code: "CR PALM",
		name: "Crèche PALMERSTON",
		shortName: "CR PALMERSTON",
		comments: "G. Cammarata (02-29) 95.664",
		year: null,
		dtDebut: "19/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 109,
	addressAdm: "PALM 3G/19",
	capacity: 12,
	name: "CYGNE",
	phone: "65116",
	dtDebut: "01/09/2001",
	dtFin: "01/01/9999",
	site: {
		siteId: 11,
		code: "CR PALM",
		name: "Crèche PALMERSTON",
		shortName: "CR PALMERSTON",
		comments: "G. Cammarata (02-29) 95.664",
		year: null,
		dtDebut: "19/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 126,
	addressAdm: "PALM 4G23",
	capacity: 10,
	name: "PLUTON",
	phone: "65110",
	dtDebut: "01/01/2000",
	dtFin: "01/01/9999",
	site: {
		siteId: 11,
		code: "CR PALM",
		name: "Crèche PALMERSTON",
		shortName: "CR PALMERSTON",
		comments: "G. Cammarata (02-29) 95.664",
		year: null,
		dtDebut: "19/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 127,
	addressAdm: "PALM 4G/23",
	capacity: 12,
	name: "PETITE OURSE",
	phone: "65124",
	dtDebut: "01/01/2000",
	dtFin: "01/01/9999",
	site: {
		siteId: 11,
		code: "CR PALM",
		name: "Crèche PALMERSTON",
		shortName: "CR PALMERSTON",
		comments: "G. Cammarata (02-29) 95.664",
		year: null,
		dtDebut: "19/09/2001",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 158,
	addressAdm: null,
	capacity: 20,
	name: "TREFLE - Tous enfants",
	phone: null,
	dtDebut: "01/05/2004",
	dtFin: "01/01/9999",
	site: {
		siteId: 14,
		code: "CR TREFLE",
		name: "Crèche TREFLE 4 FEUILLES",
		shortName: "CR TREFLE 4F",
		comments: "H. Messalkhere (02/298 84 59)",
		year: null,
		dtDebut: "01/05/2004",
		dtFin: "31/01/2007",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 51,
	addressAdm: null,
	capacity: null,
	name: "FROISSART",
	phone: null,
	dtDebut: "01/06/2006",
	dtFin: "01/01/9999",
	site: {
		siteId: 16,
		code: "CR FROISS",
		name: "Crèche FROISSART",
		shortName: "CR FROISSART",
		comments: "M. Grobovenko (022963791)",
		year: null,
		dtDebut: "01/06/2006",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 171,
	addressAdm: "Grange",
	capacity: null,
	name: "Baby room (Age 0-1)",
	phone: null,
	dtDebut: "01/04/2006",
	dtFin: "01/01/9999",
	site: {
		siteId: 19,
		code: "CR GRANGE",
		name: "Crèche GRANGE",
		shortName: "CR GRANGE",
		comments: "A-M. Lopez (02 / 298 19 04) ",
		year: null,
		dtDebut: "01/03/2006",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 173,
	addressAdm: "Grange",
	capacity: null,
	name: "Pre-School room (Age 3-5)",
	phone: null,
	dtDebut: "11/04/2006",
	dtFin: "01/01/9999",
	site: {
		siteId: 19,
		code: "CR GRANGE",
		name: "Crèche GRANGE",
		shortName: "CR GRANGE",
		comments: "A-M. Lopez (02 / 298 19 04) ",
		year: null,
		dtDebut: "01/03/2006",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 177,
	addressAdm: null,
	capacity: 15,
	name: "ABEILLE GRANDS",
	phone: null,
	dtDebut: "15/10/2006",
	dtFin: "01/01/9999",
	site: {
		siteId: 20,
		code: "CR CONS",
		name: "Crèche CONSEIL",
		shortName: "CR CONSEIL",
		comments: "G. Cammarata (02-29) 95.664",
		year: null,
		dtDebut: "15/10/2006",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 180,
	addressAdm: null,
	capacity: 15,
	name: "COCCINELLE GRANDS",
	phone: null,
	dtDebut: "15/10/2006",
	dtFin: "01/01/9999",
	site: {
		siteId: 20,
		code: "CR CONS",
		name: "Crèche CONSEIL",
		shortName: "CR CONSEIL",
		comments: "G. Cammarata (02-29) 95.664",
		year: null,
		dtDebut: "15/10/2006",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 181,
	addressAdm: null,
	capacity: 15,
	name: "PAPILLON PETITS",
	phone: null,
	dtDebut: "15/10/2006",
	dtFin: "01/01/9999",
	site: {
		siteId: 20,
		code: "CR CONS",
		name: "Crèche CONSEIL",
		shortName: "CR CONSEIL",
		comments: "G. Cammarata (02-29) 95.664",
		year: null,
		dtDebut: "15/10/2006",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 191,
	addressAdm: null,
	capacity: 12,
	name: "CERISIER",
	phone: null,
	dtDebut: "27/08/2007",
	dtFin: "01/01/9999",
	site: {
		siteId: 22,
		code: "CR GENEVE",
		name: "Crèche GENEVE",
		shortName: "CR GENEVE",
		comments: "Grobovenko Nadia (02/296.37.91)",
		year: null,
		dtDebut: "01/09/2007",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 192,
	addressAdm: null,
	capacity: 18,
	name: "JE",
	phone: null,
	dtDebut: "01/09/2007",
	dtFin: "01/01/9999",
	site: {
		siteId: 22,
		code: "CR GENEVE",
		name: "Crèche GENEVE",
		shortName: "CR GENEVE",
		comments: "Grobovenko Nadia (02/296.37.91)",
		year: null,
		dtDebut: "01/09/2007",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 208,
	addressAdm: null,
	capacity: null,
	name: "FRANKLIN",
	phone: null,
	dtDebut: "01/06/2006",
	dtFin: "01/01/9999",
	site: {
		siteId: 24,
		code: "CR FRANKL",
		name: "Crèche FRANKLIN",
		shortName: "CR FRANKLIN",
		comments: "Mme P. Caroes  (02/295.72.64)",
		year: null,
		dtDebut: "01/06/2006",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 238,
	addressAdm: null,
	capacity: null,
	name: "MARIE LOUISE",
	phone: null,
	dtDebut: "01/01/2011",
	dtFin: "01/01/9999",
	site: {
		siteId: 25,
		code: "CR M LOU",
		name: "Crèche MARIE LOUISE",
		shortName: "CR MARIE LOUISE",
		comments: "P.Caroes (02/2957264)",
		year: null,
		dtDebut: "01/12/2010",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 421,
	addressAdm: null,
	capacity: null,
	name: "545454",
	phone: null,
	dtDebut: "13/01/2015",
	dtFin: "01/01/9999",
	site: {
		siteId: 25,
		code: "CR M LOU",
		name: "Crèche MARIE LOUISE",
		shortName: "CR MARIE LOUISE",
		comments: "P.Caroes (02/2957264)",
		year: null,
		dtDebut: "01/12/2010",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 256,
	addressAdm: null,
	capacity: 12,
	name: "CANNELLE",
	phone: null,
	dtDebut: "17/01/2011",
	dtFin: "01/01/9999",
	site: {
		siteId: 26,
		code: "CR COLE",
		name: "Crèche COLE",
		shortName: "CR COLE",
		comments: "Mme Magali JESPERS - 02/296 94 61",
		year: null,
		dtDebut: "01/11/2010",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 261,
	addressAdm: null,
	capacity: 12,
	name: "ORIGAN",
	phone: null,
	dtDebut: "17/01/2011",
	dtFin: "01/01/9999",
	site: {
		siteId: 26,
		code: "CR COLE",
		name: "Crèche COLE",
		shortName: "CR COLE",
		comments: "Mme Magali JESPERS - 02/296 94 61",
		year: null,
		dtDebut: "01/11/2010",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}, {
	groupeId: 257,
	addressAdm: null,
	capacity: 12,
	name: "GIROFLE",
	phone: null,
	dtDebut: "17/01/2011",
	dtFin: "01/01/9999",
	site: {
		siteId: 26,
		code: "CR COLE",
		name: "Crèche COLE",
		shortName: "CR COLE",
		comments: "Mme Magali JESPERS - 02/296 94 61",
		year: null,
		dtDebut: "01/11/2010",
		dtFin: "01/01/9999",
		typeSite: {
			typeSiteId: 1,
			name: "Crèche",
			itemType: ESelectable.TYPE_SITE
		},
		itemType: ESelectable.SITE
	},
	itemType: ESelectable.GROUP
}];