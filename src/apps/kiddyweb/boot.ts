import {bootstrap} from 'angular2/platform/browser'
import {provide} from 'angular2/core';
import {AppComponent} from './app.component'
import {PeopleService}  from './services/person/person.service';
import {SitesService}   from './services/site/site.service';
import {HouseholdService} from './services/household/household.service';
import {ServiceManager} from './services/kw-service.service';

bootstrap(AppComponent, [
	PeopleService, 
	SitesService, 
	HouseholdService,
	ServiceManager
]);