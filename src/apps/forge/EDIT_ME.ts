import {Component, Input, Inject} from 'angular2/core';
import {IForgeApp} from './services/forgeapp';
import {Alert} from '../../components/alert/alert';
import {Tabs, Tab} from '../../components/tabs/tabs';
import {PageChild} from '../loustic/pages/child/c';

@Component({
  selector: 'forge-contained-component',
  providers: [],
  templateUrl: 'apps/forge/EDIT_ME.html',
  // template: '<' + selector + '></' + selector + '>',
  directives: [Alert, Tabs, Tab, PageChild],
  pipes: []
})
export class CMPTestedComponent {
    constructor(@Inject('IForgeApp') private forgeApp: IForgeApp) {

    }
}
