import {EventEmitter} from 'angular2/core';

export const MAX_CALLS = 5000; // Number maximum of calls to keep in the call log of
                              // a proxy
/**
 * Represents a call to a service, encapsulating the call and the result
 */
export interface IServiceCall {
    target: string; // Name of the object receiving this call
    time: string; // Time (in ms) when the call was done
    method: string; // Name of the method being invoked in this call
    params: any; // arguments passed to the method
    result: any; // result of the invocation to the service
}

export interface IServiceMonitor {
    _SP_Subscribe(generatorOrNext?: any): any;
    _SP_LastCalls(n: number): IServiceCall[];
}

/**
 * Interface of a proxy wrapper to a list of implementations to a given service
 */
export interface IServiceProxy extends IServiceMonitor {
    _SP_SetImplementation(newImpl: any);
    _SP_ProxyImplementations(): any[];
}

/**
 * Implementation of the IServiceProxy interface which is used in the function
 * createServiceProxy
 */
class ServiceProxy implements IServiceProxy {
    private __lastCalls_: IServiceCall[] = [];
    private __currImpl_: any;
    private __impls_: any[];
    private __event_Emitter_: EventEmitter<IServiceCall> = new EventEmitter();

    constructor(implementations: any[]) {
        this.__impls_ = implementations;
        this.__currImpl_ = implementations[0];
    }

    _SP_LastCalls(n: number): IServiceCall[] {
        let nelms = this.__lastCalls_.length;
        let cut = Math.min(n, nelms);
        return this.__lastCalls_.reverse().splice(cut);
    }

    _SP_AddCall(call: IServiceCall) {
        this.__lastCalls_.push(call);

        if (this.__lastCalls_.length > MAX_CALLS) {
            this.__lastCalls_ = this.__lastCalls_.slice(1);
        }

        this.__event_Emitter_.emit(call);
    }

    _SP_SetImplementation(newImpl: any) {
        this.__currImpl_ = newImpl;
    }

    _SP_ProxyImplementations(): any[] {
        return this.__impls_;
    }

    _SP_Subscribe(generatorOrNext?: any): any {
        return this.__event_Emitter_.subscribe(generatorOrNext);
    }
}

/**
 * Creates an implementation of IServiceProxy which wraps the list of
 * implementations of the service passed. The proxy allows the client to
 * chose the implementation of the service at runtime. It keeps a log of all
 * the calls to the service in the form of IServiceCall objects
 */
export function createServiceProxy (...implementations) {
    let ps:any = new ServiceProxy(implementations);
    let service = implementations[0];

    Object.getOwnPropertyNames(service).forEach( (p) => {
        ps[p] = service[p];
    });

    Object.getOwnPropertyNames(service.__proto__).forEach( (m) => {
        ps.__proto__[m] = (...args) => {
            let time = new Date();
            let tstamp = time.getMinutes() + ':' + time.getSeconds() +':' + time.getMilliseconds();
            let ret: any;
            let target: any = ps.__currImpl_;
            ret = target.__proto__[m].apply(target, args);
            ps._SP_AddCall({
                'target': target.constructor.name,
                'time': tstamp,
                'method': m,
                'params': args,
                'result': ret
            });
            return ret;
        };
    });

    return ps;
}

/**
 * Utility function which allows to create a logging proxy to any object, which
 * Creates a proxied object which log the calls to any method to the console
 */
export function createLoggedObjectProxy(service: any): any {
    return this.createObjectProxy (
        service,
        (method: string, ...args) => {
            console.log(method + 'method called...');
        },
        (retObject?: any) => {
            console.log('... returning ' + retObject);
        }
    );
}

/**
 * Creates s proxy for a given object, with the same methods and properties
 * than the original object. The callback 'doBefore' will be called before
 * any execution of any method. The callback 'doAfter' will be called
 * after the execution to any method.
 */
export function createObjectProxy (
    service: any,
    doBefore: (method: string, ...args) => void,
    doAfter: (retObject?: any) => void
) {
    let cached = service;
    let ps:any = {};

    Object.getOwnPropertyNames(service).forEach( (p) => {
        ps[p] = service[p];
    });

    Object.getOwnPropertyNames(service.__proto__).forEach( (m) => {
        ps.__proto__[m] = (...args) => {
            let ret: any;
            doBefore(m);
            ret = cached.__proto__[m].apply(cached, args);
            doAfter(ret);
            return ret;
        };
    });

    return ps;
}
