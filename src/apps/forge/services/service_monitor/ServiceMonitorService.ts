import {EventEmitter} from 'angular2/core';
import {MAX_CALLS, IServiceMonitor, IServiceCall} from './service_proxies'

/**
 * A service which monitors other service_proxies
 */
export interface IServiceMonitorService extends IServiceMonitor {
    _SP_Proxies(): any[];
}


export class ServiceMonitorService implements IServiceMonitorService {
    private _monitors: IServiceMonitor[];
    private _lastCalls: IServiceCall[] = [];
    private _eventEmitter: EventEmitter<IServiceCall> = new EventEmitter();

    constructor(monitors: IServiceMonitor[]) {
        let that = this;
        this._monitors = monitors;
        this._monitors.forEach( (mon) => {
            mon._SP_Subscribe((sc) => {
                that._onCall(sc);
            });
        });
    }

    _SP_LastCalls(n: number): IServiceCall[] {
        let nelms = this._lastCalls.length;
        let cut = Math.min(n, nelms);
        return this._lastCalls.reverse().splice(cut);
    }

    _SP_Proxies(): IServiceMonitor[] {
        return this._monitors;
    }

    _SP_Subscribe(generatorOrNext: any) {
        return this._eventEmitter.subscribe(generatorOrNext);
    }

    private _onCall(call: IServiceCall) {
        this._lastCalls.push(call);
        if (this._lastCalls.length > MAX_CALLS) {
            this._lastCalls = this._lastCalls.splice(1);
        }
        this._eventEmitter.emit(call);
    }
}
