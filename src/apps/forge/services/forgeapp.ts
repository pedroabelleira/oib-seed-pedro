export interface IForgeApp {
    onEvent(name: string, event: any): void;
}

export class ForgeApp {
    private _listeners = [];

    addListener(onEvent: (name: string, event: any) => void) {
        this._listeners.push(onEvent);
    }

    onEvent(name: string, event: any): void {
        this._listeners.forEach( (ls) => {
            ls.call(ls, name, event);
        });
    }
}
