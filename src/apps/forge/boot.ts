/// <reference path="../../typings/tsd.d.ts" />

import {bootstrap} from 'angular2/platform/browser';
import {provide, Inject, Injectable} from 'angular2/core';
import {ROUTER_PROVIDERS, APP_BASE_HREF, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {App} from './app';

/** Service interfaces */
import {ILoustic, Loustic} from '../loustic/api';
import {ITranslator} from '../../services/translation/api';
import {IGuardian} from '../../services/security/api';
import {IUserPreferences} from '../../services/preferences/api';
import {IChildrenService} from '../loustic/services/repository/api';

/** Service implementations for development */
import {TranslatorNull} from '../../services/translation/mock/TranslatorNull';
import {GuardianMock} from '../../services/security/mock/GuardianMock';
import {UserPreferencesMock} from '../../services/preferences/mock';
import {ChildrenServiceMock} from '../loustic/services/repository/mock';

import {createServiceProxy, IServiceProxy} from './services/service_monitor/service_proxies';
import {IServiceMonitorService, ServiceMonitorService} from './services/service_monitor/ServiceMonitorService';
import {IForgeApp, ForgeApp} from './services/forgeapp';

// Service configuration
let ts: IServiceProxy = createServiceProxy(new TranslatorNull());
let gs: IServiceProxy = createServiceProxy(new GuardianMock());
let ups: IServiceProxy = createServiceProxy(new UserPreferencesMock());
let cs: IServiceProxy = createServiceProxy(new ChildrenServiceMock());
let ms: IServiceMonitorService = new ServiceMonitorService([ts, gs, ups, cs]);
let fs: IForgeApp = new ForgeApp();

bootstrap(App, [
  // provide(APP_BASE_HREF, { useValue: '<%= APP_BASE + APP_DEST %>' } ),
  ROUTER_PROVIDERS,
  provide(LocationStrategy, { useClass: HashLocationStrategy }),
  provide('ITranslator', { useValue: ts}),
  provide('IGuardian', { useValue: gs}),
  provide('IUserPreferences', { useValue: ups}),
  provide('IChildrenService', { useValue: cs}),
  provide('IServiceMonitorService', { useValue: ms}),
  provide('IForgeApp', { useValue: fs}),
  provide('ILoustic', { useClass: Loustic })
]);
