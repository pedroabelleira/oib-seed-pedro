import {Component} from 'angular2/core';
import {RouteConfig, RouterOutlet, ROUTER_PROVIDERS} from 'angular2/router';
import {CMPNavigation} from './navigation/navigation';
import {PageHome} from './pages/home/home';
import {PageServices} from './pages/services/services';
import {PageSecurity} from './pages/security/security';
import {CMPBoxServices} from './pages/home/box_services/box_services';
import {CMPOutputs} from './pages/home/outputs/outputs';
import {Alert} from '../../components/alert/alert';

@Component({
  selector: 'forge',
  providers: [],
  templateUrl: 'apps/forge/app.html',
  styleUrls: ['apps/forge/app.css'],
  directives: [RouterOutlet, Alert, CMPNavigation, CMPBoxServices, CMPOutputs, PageHome, PageServices],
  pipes: []
})
@RouteConfig([
    { path: '/',                    name: 'Home',               component: PageHome},
    { path: '/services',            name: 'Services',           component: PageServices},
    { path: '/services/guardian',   name: 'GuardianService',    component: PageSecurity},
    { path: '/services/translator', name: 'TranslationService', component: PageHome},
    { path: '/services/favourites', name: 'FavouritesService',  component: PageHome}
])
export class App {
}
