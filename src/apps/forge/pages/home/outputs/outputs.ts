import {Component, Inject} from 'angular2/core';
import {ForgeApp} from '../../../services/forgeapp';

@Component({
  selector: 'forge-outputs',
  providers: [],
  templateUrl: 'apps/forge/pages/home/outputs/outputs.html',
  styleUrls: ['apps/forge/pages/home/outputs/outputs.css'],
  directives: [ ],
  pipes: []
})
export class CMPOutputs {
    private _events = [];
    private _lastevents = [];

    constructor(@Inject('IForgeApp') private forgeApp: ForgeApp) {
        let that = this;
        forgeApp.addListener( (name: string, event: any) => {
            let t = new Date();
            let ts = t.getMinutes() + ':' + t.getSeconds() + ':' + t.getMilliseconds();
            let entry = {time: ts, name: name, event: event};
            that._events.push(entry);

            that._lastevents.splice(0, 0, entry);
            while (that._lastevents.length > 10) {
                that._lastevents.pop();
            }
        });
    }
}
