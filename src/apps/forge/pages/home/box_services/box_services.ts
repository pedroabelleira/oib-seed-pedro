import {Component, Input, Inject} from 'angular2/core';
import {IServiceMonitorService} from '../../../services/service_monitor/ServiceMonitorService';
import {IServiceCall} from '../../../services/service_monitor/service_proxies';

@Component({
  selector: 'forge-services',
  providers: [],
  templateUrl: 'apps/forge/pages/home/box_services/box_services.html',
  styleUrls: ['apps/forge/pages/home/box_services/box_services.css'],
  directives: [ ],
  pipes: []
})
export class CMPBoxServices{
    private _allcalls: IServiceCall[] = [];
    private _lastcalls: IServiceCall[] = [];

    constructor(@Inject('IServiceMonitorService') private monitor: IServiceMonitorService) {
        this.monitor._SP_Subscribe( (call: IServiceCall) => {
            this._allcalls.push(call);
            this._lastcalls.splice(0, 0, call);
            while (this._lastcalls.length > 10) {
                this._lastcalls.pop();
            }
        });
    }

    ngAfterViewChecked() {
    }

    onCallClicked(call: IServiceCall) {
        alert('Call clicked: ' + call);
        return false;
    }

    isInOut(call: IServiceCall) {
        let pin: boolean = typeof call.params != 'undefined';
        let pout: boolean = typeof call.result != 'undefined';
        return pin && pout;
    }

    isJustOut(call: IServiceCall) {
        let pin: boolean = typeof call.params != 'undefined';
        let pout: boolean = typeof call.result != 'undefined';
        return !pin && pout;
    }

    isJustIn(call: IServiceCall) {
        let pin: boolean = typeof call.params != 'undefined';
        let pout: boolean = typeof call.result != 'undefined';
        return pin && !pout;
    }
}
