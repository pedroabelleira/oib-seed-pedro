import {Component, View, ViewChild, ViewQuery, Input, Output} from 'angular2/core';
import {ElementRef, ContentChildren, QueryList, EventEmitter, Inject} from 'angular2/core';
import {PageChildren} from "../../../loustic/pages/children/c";
import {TreeNode} from "../../../../components/treenode/TreeNode";
import {CMPTestedComponent} from '../../EDIT_ME';
import {PageServices} from '../../pages/services/services';
import {CMPBoxServices} from '../../pages/home/box_services/box_services';
import {CMPOutputs} from '../../pages/home/outputs/outputs';

@Component({
  selector: 'forge-cmp-container',
  providers: [],
  templateUrl: 'apps/forge/pages/home/home.html',
  styleUrls: ['apps/forge/pages/home/home.css'],
  directives: [TreeNode, CMPTestedComponent, CMPBoxServices, CMPOutputs],
  pipes: []
})
export class PageHome {
    @Output() onChildInputs = new EventEmitter();
    @Output() onChildOutputs = new EventEmitter();
    @ContentChildren(TreeNode) childrenList: QueryList<TreeNode>;

    constructor() {
    }

    ngAfterViewInit() {
        let cs = this.childrenList;
        cs.toString();
        try {
            // let inputs = Reflect.getOwnMetadata('annotations', this.child.constructor).inputs;
            // let outputs = Reflect.getOwnMetadata('annotations', this.child.constructor).outputs;
            // let out2 = Reflect.getOwnMetadata('annotations', Tabs).outputs;
            //
            // // this.onChildInputs.emit(inputs);
            // this.onChildInputs.emit('eoeoeo');
            // // this.onChildOutputs.emit(outputs);
            // this.onChildOutputs.emit('eoeoout');
            //
            // alert('Inputs: ' + inputs);
        } catch(e) {
            alert('Error: ' + e); // FIXME
        }
    }
}
