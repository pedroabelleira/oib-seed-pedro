import {Component, Input, Output, EventEmitter, Inject} from 'angular2/core';
import {RouterLink} from 'angular2/router';
import {CMPTestedComponent} from '../../EDIT_ME';
import {CMPBoxServices} from '../../pages/home/box_services/box_services';
import {CMPOutputs} from '../../pages/home/outputs/outputs';
import {CmpDevGuardianMock} from '../../../../components/devtools/guardian_mock_ui/c';
import {IForgeApp} from '../../services/forgeapp';

@Component({
    selector: 'forge-security',
    providers: [],
    templateUrl: 'apps/forge/pages/security/security.html',
    styleUrls: ['apps/forge/pages/security/security.css'],
    directives: [CMPTestedComponent, CMPBoxServices, CMPOutputs, CmpDevGuardianMock],
})
export class PageSecurity {
    constructor(
        @Inject('IForgeApp') public forge: IForgeApp)
    {}

    ngOnInit() {
        // Init code should go here
    }
}
