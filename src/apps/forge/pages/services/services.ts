import {Component, Input, Output, EventEmitter, Inject} from 'angular2/core';
import {RouterLink} from 'angular2/router';
import {CMPTestedComponent} from '../../EDIT_ME';
import {CMPBoxServices} from '../../pages/home/box_services/box_services';
import {CMPOutputs} from '../../pages/home/outputs/outputs';
import {IForgeApp} from '../../services/forgeapp';

@Component({
    selector: 'forge-services',
    providers: [],
    templateUrl: 'apps/forge/pages/services/services.html',
    styleUrls: ['apps/forge/pages/services/services.css'],
    directives: [CMPTestedComponent, CMPBoxServices, CMPOutputs],
})
export class PageServices {
    constructor(
        @Inject('IForgeApp') public forge: IForgeApp)
    {}

    ngOnInit() {
        // Init code should go here
    }
}
