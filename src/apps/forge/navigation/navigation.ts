import {Component, Inject} from 'angular2/core';
import {Alert} from '../../../components/alert/alert';
import {RouterLink} from 'angular2/router';
import {IGuardian} from '../../../services/security/api';

@Component({
  selector: 'forge-navigation',
  providers: [],
  templateUrl: 'apps/forge/navigation/navigation.html',
  styleUrls: ['apps/forge/navigation/navigation.css'],
  directives: [RouterLink],
  pipes: []
})
export class CMPNavigation{
    private _username: string;

    constructor(@Inject('IGuardian') public guardian: IGuardian) {}

    ngOnInit() {
        this._username = this.guardian.getLoggedUser().fullName;
    }
}
