import { Component, Input, Output, EventEmitter } from 'angular2/core';
import { IPeople } from '../filteryService/filtery.interface';

declare var $: any;

@Component({
  selector: 'filter-name',
  providers: [],
  templateUrl: 'app/pages/shell/filterName/filterName.html',
  styleUrls: ['app/pages/shell/filterName/filterName.css'],
  directives: [],
  pipes: []
})
export class FilterName {
  @Input() set people(people: IPeople[]) {
    // Using a setter because it's a jQuery component which has to be initialised
    // in Javascript (Duh!)
    // Destroying it before is not useful for the first assignment,
    // but needed for subsequent assignments.
    $('#autocompletion').typeahead('destroy');

    $('#autocompletion').typeahead({
      source: people,
      displayText: item => item.fullName,
      items: 15,
      autoSelect: true
    });
  }

  @Input() disabled: boolean = false;

  @Output() onPatternChanged = new EventEmitter();

  filter(pattern: string): void {
    this.onPatternChanged.emit(pattern);
  }

  filterAfterSelect(): void {
    // Uggly hack because using jQuery
    // When clicking on a element from the popup, value is not directly set into the input element,
    // we have to wait a bit...
    setTimeout(() => {
      this.onPatternChanged.emit($('#autocompletion').val());
    }, 200);
  }
}
