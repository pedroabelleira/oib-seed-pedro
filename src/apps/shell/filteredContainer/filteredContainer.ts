import { Component, Input, Output, EventEmitter } from 'angular2/core';
import { IPeople } from '../filteryService/filtery.interface';
import { PeopleList } from '../peopleList/peopleList';

@Component({
  selector: 'filtered-container',
  providers: [],
  templateUrl: 'app/pages/shell/filteredContainer/filteredContainer.html',
  styleUrls: ['app/pages/shell/filteredContainer/filteredContainer.css'],
  directives: [ PeopleList ],
  pipes: []
})
export class FilteredContainer {
  @Input() peopleGroup1: IPeople[];
  @Input() peopleGroup2: IPeople[];
  @Input() peopleGroup3: IPeople[];

  @Output() onTabChanged = new EventEmitter();

  tabChanged(tab): void {
    this.onTabChanged.emit(tab);
  }
}
