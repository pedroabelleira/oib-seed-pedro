import { Component } from 'angular2/core';
import { FilterType, EType } from './filterType/filterType';
import { FilterName } from './filterName/filterName';
import { FilteredContainer } from './filteredContainer/filteredContainer';
import { IPeople } from './filteryService/filtery.interface';
import { FilteryService } from './filteryService/filtery.service';

@Component({
  selector: 'oib-shell',
  providers: [],
  templateUrl: 'app/pages/shell/shell.html',
  styleUrls: ['app/pages/shell/shell.css'],
  directives: [ FilterType, FilterName, FilteredContainer ],
  pipes: []
})
export class Shell {
}
