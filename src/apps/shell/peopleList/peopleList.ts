import { Component, Input } from 'angular2/core';
import { IPeople } from '../filteryService/filtery.interface';

@Component({
  selector: 'people-list',
  providers: [],
  templateUrl: 'app/pages/shell/peopleList/peopleList.html',
  styleUrls: ['app/pages/shell/peopleList/peopleList.css'],
  directives: [],
  pipes: []
})
export class PeopleList {
  @Input() people: IPeople[];
}
