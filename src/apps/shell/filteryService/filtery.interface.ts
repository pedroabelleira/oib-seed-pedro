export interface IPeople {
  id: string;
  fullName: string;
  birthDate?: string;
	isFavourite?: boolean;
	job?: string;
	avatar?: string;
}

export interface IFilteryService {
	// Returns a list of random people
	getPeople(): IPeople[];
}