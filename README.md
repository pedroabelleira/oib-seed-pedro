# Angular2@OIB
Sample application showcasing a component for filtering 3 collections. The page is composed of 3 components not aware of each other.

## Installation

### Requirements

Install node (https://nodejs.org).

Install angular-cli:

```
npm install -g angular-cli
```

### Cloning

```
git clone https://olimungo@bitbucket.org/olimungo/oib-seed.git
```

### Installing

```
npm install
```

### Serving

To launch a local server which takes care of transpiling typescript into javascript files and serving the static files.

```
ng serve
```

## Main files


```
├──  dist/
├──  nodes_modules/
├──  src/
│   ├──  app/
│   │   └──  components/
│   │       └──  filteredContainer/
│   │       │   ├──  filteredContainer.css
│   │       │   ├──  filteredContainer.html
│   │       │   └──  filteredContainer.ts
│   │       │
│   │       └──  filterName/
│   │       │   ├──  filterName.css
│   │       │   ├──  filterName.html
│   │       │   └──  filterName.ts
│   │       │
│   │       └──  filterType/
│   │       │   ├──  filterType.css
│   │       │   ├──  filterType.html
│   │       │   └──  filterType.ts
│   │       │
│   │       └──  filteryService/
│   │       │   ├──  filtery.interface.ts
│   │       │   └──  filtery.service.ts
│   │       │
│   │       └──  peopleList/
│   │       │   ├──  peopleList.css
│   │       │   ├──  peopleList.html
│   │       │   └──  peopleList.ts
│   │       │
│   │       └──  shell/
│   │       │   ├──  shell.css
│   │       │   ├──  shell.html
│   │       │   └──  shell.ts
|   |
│   ├──  typings/
│   ├──  vendor/
|   |
│   ├──  app.ts
│   ├──  index.css
│   ├──  index.html
│   └──  tsdconfig.json
│
├── tmp/
├──  .gitignore
└──  package.json
``` 
  
_tmp_ is used by the local server for its building process.

_dist_ is used by the local server for transpiling typescript into javascript files. The local server actually serves files from this folder.

## Components

* shell: the main component which orchestrates the other components
* filterType: this component allows the user to select 3 types of search:
    * _All_: search will be done on all people
    * _Favourites_: search will be done on people tagged as "Favourites"
    * _Date_: search will be done on a selected date
* filterName: this component allows the user to input a pattern for filtering the people
* filteredContainer: this components displays 3 tabs. Each one contains a group of different people
* peopleList: this component receives a group of pepole and displays them. It is used in the 3 tabs of the filteredContainer
* filterService: takes care of generating fake persons using the "faker" library